# Clever-Curricula

Module to manage the components of the Clever syllabus system.
Developed with Angular (4.0.1), Firebase (3.7.5) and AngularFire (2.0.0-beta-8).

# Coding standards
-------------------------------------------------------------------------------------------
The coding standards used in the project are described below.
There are standards for TypeScript, HTML, and for the overall structure of the project.

## Typescript

### Clases
Class names begin with a capital letter.
They also use camel notation
> Example: `MyClass`

### Methods
Method names begin with lowercases
They also use camel notation
> Example: `myMethod`

### Variables
Variable names begin with lowercases
They also use camel notation
> Example: `miVariable`

## HTML
-------------------------------------------------------------------------------------------

### Identifiers
Identifiers begin with lowercases
They also use camel notation
> Example: `<component id="myIdentifier"> ... </component>`

## Firebase
----------------------------------------------------------------------------------------

### Structure of project components
Each data collection must be managed independently.
This will create a component for each collection.
In addition, each collection will have a service that will provide functions to access the database.

### Collections
For each collection, create all your files within a folder with the name of the collection
The name of the folder is written starting with lowercase and in the singular
Consider the following names for components and services

* Service
	* File: component + "service.ts"
	> Example: nivel.service.ts
	* Class: Component's name + "Service"
	> Example: NivelService
* Component to create new elements
	* File: component + "-new.component.ts"
	> Example: nivel-new.component.ts
	* Class: Component's name + "NewComponent"
	> Example: NivelNewComponent
* Component to update elements
	* File: component + "-edit.component.ts"
	> Example: nivel-edit.component.ts
	* Class: Component's name + "EditComponent"
	> Example: NivelEditComponent
* Component to display all the available elements
	* File: component + "component.ts"
	> Example: nivel.component.ts
	* Class: Component's name + "Component"
	> Example: NivelComponent
* Component to display the detailed information of an element
	* File: component + "-detail.component.ts"
	> Example: nivel-detail.component.ts
	* Class: Component's name + "DetailComponent"
	> Example: NivelDetailComponent
* Messages of a component
	* File: "/assets/" + component + "-messages.json"
	> Example: nivel-messages.json
	
### Services
Use the following names for basic CRUD (Create, Read, Update, Delete) operations within the services:

* Create: add + Componente
> Example: `addNivel`
* Read: get + Componente(s)			
> Example: `getNivel` o `getNiveles`
* Update: update + Componente		
> Example: `updateNivel`
* Delete: delete + Componente
> Example: `deleteNivel`

### Components
Use the following names for operations associated with GUI buttons:

* Previous page: goBack
> Example: `goBack()`
* Update an element
> Example: `onUpdate()`
* Create a new element
> Example: `onAdd()`
* Display the detailed information of a component
> Example: `onDetails()`
* Delete an element
> Example: `onDelete()`
		
Sometimes a component has complementary elements that are administered in the same way:

* Delete a complementary element: onDelete + Element's name
> Example: `onDeleteTema()`
* Add a new complementary element: onAdd + Element's name
> Ejemplo: `onAddTema()`
* Update complementary element: onUpdate + Element's name
> Example: `onUpdateTema()`
* Delete a complementary element: onDelete + Element's name
> Example: `onDeleteTema()`
	
### Validation
To validate use the class-validator library (https://github.com/pleerock/class-validator)

### CleverKbBase Class
All components must be child classes of the CleverKbBase class
This class contains methods to validate that a user is authenticated and to handle error messages

### Menu
The menu with the various options available to each user is located within the menu component.
Here you must activate or deactivate the available options for each type of user.

> Files: menu.component.html y menu.component.ts


# Additional information
---------------------------------------------------------------------------------------
To use the project, first create a [Firebase project](https://console.firebase.google.com/) and link it to this.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).