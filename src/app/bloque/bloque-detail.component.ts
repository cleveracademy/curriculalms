import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { MdTab, MdTabGroup } from '@angular/material';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { BloqueService, Bloque } from './bloque.service';
import { TemaService, Tema } from './tema.service';
import { CleverKbBase } from './../cleverKB-base';
import { validate, IsNotEmpty } from 'class-validator';

@Component({
  selector: 'app-bloque-detail',
  templateUrl: './bloque-detail.component.html'
})

export class BloqueDetailComponent extends CleverKbBase implements OnInit {
	bloque: Bloque;

	temas: any;
	tema: any;
	modoVisualizacion: string;

	constructor(private bloqueService: BloqueService, private temaService: TemaService,
	          	private auth: AngularFireAuth, private router: Router, 
	          	private location: Location, private route: ActivatedRoute) {
		super();
		super.validateUser(auth, router);
	}

	ngOnInit() {
    	this.route.params.subscribe((params: Params) => {
      		this.bloqueService.getbloque(params['id']).subscribe(bloque => {
        		this.bloque = bloque;
      		});
		});
	}

	goBack(): void {
		this.location.back();
	}

	/**
	 * Identifica el tema elegido de la lista de temas del bloque y obtiene sus recursos asociados
	 */
	onSelectTema(): void {
		this.temaService.getTema(this.temas[0].$key).subscribe((tema:Tema) => {
			this.tema = tema;
		});
	}
}