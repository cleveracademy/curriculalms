import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { MdTab, MdTabGroup } from '@angular/material';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { MateriaService } from './../materia/materia.service';
import { BloqueService, Bloque, Proyecto } from './bloque.service';
import { TemaService, Tema, Contenido, TipoContenidoDigital } from './tema.service';
import { EditorElementosTema, ConfiguracionEditor, Componente, Operacion } from './editorElementosTema.component';
import { CleverKbBase } from './../cleverKB-base';
import { validate, IsNotEmpty } from 'class-validator';
import { MdDialog } from '@angular/material';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-bloque-edit',
  templateUrl: './bloque-edit.component.html'
})

export class BloqueEditComponent extends CleverKbBase implements OnInit {
	materias: Observable<any>;
	tipos = TipoContenidoDigital;

	bloque: Bloque;
	tema: Tema;
	proyecto: Proyecto;
	contenido: Contenido;

	modoVisualizacion: string;

	//Campos de texto dentro de la forma
	temaTexto:string;
	aprendizajeTexto: string;
	preguntaTexto: string;
	contenidoTexto: string;
	proyectoTexto: string
	
	//Listas con los elementos seleccionados
	temas: any;
	aprendizajes: any;
	preguntas: any;
	contenidos: any;
	proyectos: any;
	aprendizajesPorValidar: any;
	preguntasPorValidar: any;
	contenidosPorValidar: any;
	proyectosPorValidar: any;

	//Mensajes de error leidos del archivo bloque-messages.json
	messages: any;
	//Mensaje de error mostrado al usuario
	message: string;

	constructor(private materiaService: MateriaService, private bloqueService: BloqueService, private temaService: TemaService,
	          	private auth: AngularFireAuth, private route: ActivatedRoute, 
	          	private location: Location, private router: Router, private http: Http, 
	          	private temaDialog: MdDialog, private bloqueDialog: MdDialog) {
		super();
		super.validateUser(auth, router);
	}

	goBack(): void {
		this.location.back();
	}

	ngOnInit() {
		//Obtiene los mensajes antes de visualizar la página
		this.route.data.subscribe(data => {
	      this.messages = data.messages.json();
	    });

		//Obtiene las materias disponibles
		this.materiaService.getMaterias().subscribe(materias => this.materias = materias);
	    
	    //Obtiene el bloque a ser editado	
    	this.route.params.subscribe((params: Params) => {
      		this.bloqueService.getbloque(params['id']).subscribe(bloque => {
        		this.bloque = bloque;
      		});
		});

		this.proyecto = new Proyecto();
		this.contenido = new Contenido();
	}

	/**
	 * Actualiza dentro de la base los datos del bloque (nombre, materia y descripcion)
	 */
	onUpdate(): void {
		validate(this.bloque).then(errors => {
      		if (errors.length > 0) {
        		this.message = super.extractErrorMessages(errors);
      		}
			else {
			    this.bloqueService.update(this.bloque);
				this.router.navigate(['/bloques']);
		  	}
    	});
	}

	////////////////////////////////////////// TEMAS /////////////////////////////////////////////////////////
	/**
	 * Agrega nuevos temas a la base. 
	 * @param texto El texto que será partido para identificar los temas
	 */
	private addTema(texto: string): void {
		if (super.isNotEmpty(texto)) {
			let lineas = texto.split('\n');
			for (let linea of lineas) {
				if (super.isNotEmpty(linea))
		  			this.bloqueService.addBloqueTema(this.bloque.idBloque, linea);
			}
			this.temaTexto = "";
		}
		else
			this.message = this.messages['errorAddTema'];	
	}

	/**
	 * Abre la ventana emergente para agregar un nuevo tema
	 */
	onAddTema(): void {
		let configuracion = {operacion: Operacion.Add, componente: Componente.Tema};
		let dialogRef = this.temaDialog.open(EditorElementosTema, {data: configuracion });
		dialogRef.afterClosed().subscribe(result => {
			if (result == 'add') {
				this.addTema(dialogRef.componentInstance.tema);
			}
		});
	}

	/** 
	 * Abre una ventana para que se edite la información del primer tema seleccionado
	 */
	onEditTema(): void {
		let configuracion = {operacion: Operacion.Edit, componente: Componente.Tema, tema: this.temas[0].tema };
		let dialogRef = this.temaDialog.open(EditorElementosTema, { data: configuracion});
		dialogRef.afterClosed().subscribe(result => {
			if (result == 'add' && super.isNotEmpty(dialogRef.componentInstance.tema)) {
				this.bloqueService.updateBloqueTema(this.temas[0].$key, dialogRef.componentInstance.tema);
			}
		});
	}

	/**
	 * Elimina los temas seleccionados de la lista this.temas
	 */
	onDeleteTema(): void {
		if (super.isNotEmpty(this.temas)) {
		  for (let miTema of this.temas) {
		  	let result = confirm("Eliminar permanentemente el tema " + miTema['tema']);
  			if (result)
		  		this.bloqueService.deleteBloqueTema(miTema.$key);
		  }
		}
		else
		  this.message = this.messages['errorDeleteTema'];
	}

	/**
	 * Identifica el tema elegido de la lista de temas del bloque y obtiene sus recursos asociados
	 */
	onSelectTema(): void {
		this.temaService.getTema(this.temas[0].$key).subscribe((tema:Tema) => {
			this.tema = tema;
		});
	}

	////////////////////////////////////// APRENDIZAJES ESPERADOS//////////////////////////////////////////////////////
	/**
	 * Extrae todos los aprendizajes escritos en el área de texto aprendizajeTexto
	 * Cada aprendizaje es separado por un salto de línea
	 */
	onAddAprendizaje(): void {
		if (super.isNotEmpty(this.aprendizajeTexto)) {
			let texto = this.aprendizajeTexto.split('\n');
			for (let a of texto) {
				if (super.isNotEmpty(a)) {
		  			this.bloqueService.addBloqueAprendizaje(this.bloque.idBloque, {aprendizaje: a});
				}
			}
			this.aprendizajeTexto = "";
		}
		else
			this.message = this.messages['errorAddAprendizaje'];
	}

	/**
	 * Elimina los temas seleccionados de la lista this.aprendizajes
	 */
	onDeleteAprendizaje(): void {
		this.deleteAprendizajes(this.aprendizajes);
	}

	/**
	 * Remueve los aprendizajes seleccionados de la lista aprendizajes por validar
	 */
	onDeleteAprendizajePorValidar(): void {
		this.deleteAprendizajes(this.aprendizajesPorValidar);
	}

	deleteAprendizajes(lista): void {
		if (super.isNotEmpty(lista)) {
		  for (let miAprendizaje of lista) {
		  	let result = confirm("Eliminar permanentemente el aprendizaje " + miAprendizaje['aprendizaje']);
  			if (result)
  				this.bloqueService.deleteBloqueAprendizaje(miAprendizaje.$key);
		  }
		}
		else
		  this.message = this.messages['errorDeleteAprendizaje'];	
	}

	onEditAprendizajePorValidar(): void {
		let configuracion = {operacion: Operacion.Edit, componente: Componente.Aprendizaje, aprendizaje: this.aprendizajesPorValidar[0].aprendizaje };
		this.editAprendizaje(configuracion, this.aprendizajesPorValidar[0].$key);
	}

	onEditAprendizaje(): void {
		let configuracion = {operacion: Operacion.Edit, componente: Componente.Aprendizaje, aprendizaje: this.aprendizajes[0].aprendizaje };
		this.editAprendizaje(configuracion, this.aprendizajes[0].$key);
	}

	editAprendizaje(configuracion: ConfiguracionEditor, idAprendizaje: string): void {
		let dialogRef = this.temaDialog.open(EditorElementosTema, { data: configuracion });
		dialogRef.afterClosed().subscribe(result => {
			if (result == 'add' && super.isNotEmpty(dialogRef.componentInstance.aprendizaje)) {
				this.bloqueService.updateBloqueAprendizaje(idAprendizaje, {aprendizaje: dialogRef.componentInstance.aprendizaje});
			}
		});	
	}

	/**
	 * Valida los aprendizajes seleccionados de la lista de aprendizajes por validar, y los pasa a la lista de validados
	 */
	onValidateAprendizaje(): void {
		if (super.isNotEmpty(this.aprendizajesPorValidar)) {
			for (let miAprendizaje of this.aprendizajesPorValidar) {
				this.bloqueService.validateBloqueAprendizaje(miAprendizaje.$key);
			}
		}
		else
	  	    this.message = this.messages['errorValidateAprendizaje'];
	}

	////////////////////////////////////// PREGUNTAS SOLE //////////////////////////////////////////////////////
	/**
	 * Extrae todos los aprendizajes escritos en el área de texto aprendizajeTexto
	 * Cada aprendizaje es separado por un salto de línea
	 */
	onAddPregunta(): void {
		if (super.isNotEmpty(this.preguntaTexto)) {
			let texto = this.preguntaTexto.split('\n');
			for (let ps of texto) {
				if (super.isNotEmpty(ps)) {
		  			this.temaService.addTemaPregunta(this.tema.idTema, ps);
				}
			}
			this.preguntaTexto = "";
		}
		else
			this.message = this.messages['errorAddPregunta'];
	}

	/**
	 * Elimina los temas seleccionados de la lista this.aprendizajes
	 */
	onDeletePregunta(): void {
		this.deletePreguntas(this.preguntas);
	}

	/**
	 * Remueve los aprendizajes seleccionados de la lista aprendizajes por validar
	 */
	onDeletePreguntaPorValidar(): void {
		this.deletePreguntas(this.preguntasPorValidar);
	}

	deletePreguntas(lista): void {
		if (super.isNotEmpty(lista)) {
		  for (let miPregunta of lista) {
		  	let result = confirm("Eliminar permanentemente la pregunta " + miPregunta['pregunta']);
  			if (result)
  				this.temaService.deleteTemaPregunta(miPregunta.$key);
		  }
		}
		else
		  this.message = this.messages['errorDeletePregunta'];	
	}

	onEditPreguntaPorValidar(): void {
		let configuracion = {operacion: Operacion.Edit, componente: Componente.Pregunta, pregunta: this.preguntasPorValidar[0].pregunta };
		this.editPregunta(configuracion, this.preguntasPorValidar[0].$key);
	}

	onEditPregunta(): void {
		let configuracion = {operacion: Operacion.Edit, componente: Componente.Pregunta, pregunta: this.preguntas[0].pregunta };
		this.editPregunta(configuracion, this.preguntas[0].$key);
	}

	editPregunta(configuracion: ConfiguracionEditor, idPregunta: string): void {
		let dialogRef = this.temaDialog.open(EditorElementosTema, { data: configuracion });
		dialogRef.afterClosed().subscribe(result => {
			if (result == 'add' && super.isNotEmpty(dialogRef.componentInstance.pregunta)) {
				this.temaService.updateTemaPregunta(idPregunta, dialogRef.componentInstance.pregunta);
			}
		});	
	}

	/**
	 * Valida los aprendizajes seleccionados de la lista de aprendizajes por validar, y los pasa a la lista de validados
	 */
	onValidatePregunta(): void {
		if (super.isNotEmpty(this.preguntasPorValidar)) {
			for (let miPregunta of this.preguntasPorValidar) {
				this.temaService.validateTemaPregunta(miPregunta.$key);
			}
			this.message = "";
		}
		else
	  	    this.message = this.messages['errorValidatePregunta'];
	}

   //* :::::::::: CONTENIDO DIGITAL :::::::::: */
	onAddContenido(): void {
		validate(this.contenido).then(errors => {
			if (errors.length > 0) {
				this.message = super.extractErrorMessages(errors);
			}
			else {
				this.temaService.addTemaContenido(this.tema.idTema, this.contenido);
	      		this.contenido.titulo = "";
	      		this.contenido.URL = "";
	      		this.message = "";
	      	}
		});
	}
	
	/**
	 * Elimina los temas seleccionados de la lista this.contenidos
	 */
	onDeleteContenido(): void {
		this.deleteContenidos(this.contenidos);
	}

	/**
	 * Remueve los contenidos seleccionados de la lista aprendizajes por validar
	 */
	onDeleteContenidoPorValidar(): void {
		this.deleteContenidos(this.contenidosPorValidar);
	}

	deleteContenidos(lista): void {
		if (super.isNotEmpty(lista)) {
		  for (let miContenido of lista) {
		  	let result = confirm("Eliminar permanentemente el contenido " + miContenido['titulo']);
  			if (result)
  				this.temaService.deleteTemaContenido(miContenido.$key);
		  }
		  this.message = "";
		}
		else
		  this.message = this.messages['errorDeleteContenido'];	
	}

	onEditContenidoPorValidar(): void {
		let configuracion = {operacion: Operacion.Edit, componente: Componente.ContenidoDigital, 
			tituloContenido: this.contenidos[0].titulo,
			URL: this.contenidos[0].URL,
			tipoContenido: this.contenidos[0].tipo
		};
			
		this.editContenido(configuracion, this.contenidosPorValidar[0].$key);
	}

	onEditContenido(): void {
		let configuracion = {operacion: Operacion.Edit, componente: Componente.ContenidoDigital, 
			tituloContenido: this.contenidos[0].titulo,
			URL: this.contenidos[0].URL,
			tipoContenido: this.contenidos[0].tipo
		};
		this.editContenido(configuracion, this.contenidos[0].$key);
	}

	editContenido(configuracion: ConfiguracionEditor, idContenido: string): void {
		let dialogRef = this.temaDialog.open(EditorElementosTema, { data: configuracion });
		dialogRef.afterClosed().subscribe(result => {
			if (result == 'add' && super.isNotEmpty(dialogRef.componentInstance.tituloContenido)) {
				let contenido = new Contenido();
				contenido.titulo = dialogRef.componentInstance.tituloContenido;
				contenido.URL = dialogRef.componentInstance.URL;
				contenido.tipo = dialogRef.componentInstance.tipoContenido;
				this.temaService.updateTemaContenido(idContenido, contenido);
			}
		});	
	}

	/**
	 * Valida los contenidos seleccionados de la lista de aprendizajes por validar, y los pasa a la lista de validados
	 */
	onValidateContenido(): void {
		if (super.isNotEmpty(this.contenidosPorValidar)) {
			for (let miContenido of this.contenidosPorValidar) {
				this.temaService.validateTemaContenido(miContenido.$key);
			}
			this.message = "";
		}
		else
	  	    this.message = this.messages['errorValidateContenido'];
	}
   //* :::::::::: PROYECTOS :::::::::: */
	onAddProyecto(): void {
		validate(this.proyecto).then(errors => {
      		if (errors.length > 0) {
        		this.message = super.extractErrorMessages(errors);
      		}
      		else {
				this.bloqueService.addBloqueProyecto(this.bloque.idBloque, this.proyecto);
	      		this.proyecto.titulo = "";
	      		this.proyecto.planteamiento = "";
	      		this.proyecto.investigacion = "";
	      		this.proyecto.planeacion = "";
	      		this.proyecto.creacion = "";
	      		this.proyecto.presentacion = "";
	      		this.proyecto.creacion = "";
	      		this.message = "";
	      	}
      	});
	}

	/**
	 * Elimina los temas seleccionados de la lista this.proyectos
	 */
	onDeleteProyecto(): void {
		this.deleteProyectos(this.proyectos);
	}

	/**
	 * Remueve los contenidos seleccionados de la lista aprendizajes por validar
	 */
	onDeleteProyectoPorValidar(): void {
		this.deleteProyectos(this.proyectosPorValidar);
	}

	deleteProyectos(lista): void {
		if (super.isNotEmpty(lista)) {
		  for (let miProyecto of lista) {
		  	let result = confirm("Eliminar permanentemente el proyecto " + miProyecto['titulo']);
  			if (result)
  				this.bloqueService.deleteBloqueProyecto(miProyecto.$key);
		  }
		  this.message = "";
		}
		else
		  this.message = this.messages['errorDeleteProyecto'];	
	}

	onEditProyectoPorValidar(): void {
		let configuracion = {operacion: Operacion.Edit, componente: Componente.Proyecto,
			tituloProyecto: this.proyectosPorValidar[0].titulo,
			planteamientoProyecto: this.proyectosPorValidar[0].planteamiento,
			investigacionProyecto: this.proyectosPorValidar[0].investigacion,
			planeacionProyecto: this.proyectosPorValidar[0].planeacion,
			creacionProyecto: this.proyectosPorValidar[0].creacion,
			presentacionProyecto: this.proyectosPorValidar[0].presentacion
		};

		this.editProyecto(configuracion, this.proyectosPorValidar[0].$key);
	}

	onEditProyecto(): void {
		let configuracion = {operacion: Operacion.Edit, componente: Componente.Proyecto,
			tituloProyecto: this.proyectos[0].titulo,
			planteamientoProyecto: this.proyectos[0].planteamiento,
			investigacionProyecto: this.proyectos[0].investigacion,
			planeacionProyecto: this.proyectos[0].planeacion,
			creacionProyecto: this.proyectos[0].creacion,
			presentacionProyecto: this.proyectos[0].presentacion
		};

		this.editProyecto(configuracion, this.proyectos[0].$key);
	}

	editProyecto(configuracion: ConfiguracionEditor, idProyecto: string): void {
		let dialogRef = this.bloqueDialog.open(EditorElementosTema, { data: configuracion });
		dialogRef.afterClosed().subscribe(result => {
			if (result == 'add' && super.isNotEmpty(dialogRef.componentInstance.tituloProyecto)) {
				let proyecto = new Proyecto();
				proyecto.titulo = dialogRef.componentInstance.tituloProyecto;
				proyecto.planteamiento = dialogRef.componentInstance.planteamientoProyecto;
				proyecto.investigacion = dialogRef.componentInstance.investigacionProyecto;
				proyecto.planeacion = dialogRef.componentInstance.planeacionProyecto;
				proyecto.creacion = dialogRef.componentInstance.creacionProyecto;
				proyecto.presentacion = dialogRef.componentInstance.presentacionProyecto;

				this.bloqueService.updateBloqueProyecto(idProyecto, proyecto);
			}
		});	
	}
	/**
	 * Valida los proyectos seleccionados de la lista de proyectos por validar, y los pasa a la lista de validados
	 */
	onValidateProyecto(): void {
		if (super.isNotEmpty(this.proyectosPorValidar)) {
			for (let miProyecto of this.proyectosPorValidar) {
				this.bloqueService.validateBloqueProyecto(miProyecto.$key);
			}
			this.message = "";
		}
		else
	  	    this.message = this.messages['errorValidateProyecto'];
	}
}