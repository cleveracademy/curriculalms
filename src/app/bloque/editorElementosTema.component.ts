import { Component, OnInit, Inject } from '@angular/core';
import { MdDialogRef, MD_DIALOG_DATA } from '@angular/material';
import { TipoContenidoDigital } from './tema.service';

@Component({
  selector: 'editorElementosTema',
  templateUrl: './editorElementosTema.component.html'
})
export class EditorElementosTema implements OnInit {
	tema: string;
	aprendizaje: string;
	pregunta: string;
	
	tituloContenido: string;
	URL: string;
	tipoContenido: string;
	tipos = TipoContenidoDigital;

	tituloProyecto: string;
	planteamientoProyecto: string;
	investigacionProyecto: string;
	planeacionProyecto: string;
	creacionProyecto: string;
	presentacionProyecto: string;

	titulo: string;
	operacion: string;

	constructor(public dialogRef: MdDialogRef<EditorElementosTema>, 
				@Inject(MD_DIALOG_DATA) public data?: ConfiguracionEditor) {
		if (data) {
			switch (data.componente) {
				case Componente.Tema: this.titulo = "Tema"; break;
				case Componente.Aprendizaje: this.titulo = "Aprendizaje"; break;
				case Componente.Pregunta: this.titulo = "Pregunta"; break;
				case Componente.ContenidoDigital: this.titulo = "Contenido Digital"; break;
				case Componente.Proyecto: this.titulo = "Proyecto"; break;
			}

			switch (data.operacion) {
				case Operacion.Add: this.operacion = "Agregar"; break;
				case Operacion.Edit: 
					this.operacion = "Editar"; 
					switch (data.componente) {
						case Componente.Tema: this.tema = data.tema; break;
						case Componente.Aprendizaje: this.aprendizaje = data.aprendizaje; break;
						case Componente.Pregunta: this.pregunta = data.pregunta; break;
						case Componente.ContenidoDigital: 
							this.tituloContenido = data.tituloContenido; 
							this.URL = data.URL;
							this.tipoContenido = data.tipoContenido;
							break;
						case Componente.Proyecto:
							this.tituloProyecto = data.tituloProyecto;
							this.planteamientoProyecto = data.planteamientoProyecto;
							this.investigacionProyecto = data.investigacionProyecto;
							this.planeacionProyecto = data.planeacionProyecto;
							this.creacionProyecto = data.creacionProyecto;
							this.presentacionProyecto = data.presentacionProyecto;
							break;
					}
					break;
			}
		}
	}

	ngOnInit() {
	}
}

export class ConfiguracionEditor {
	componente: Componente;
	operacion: Operacion;
	tema?: string;
	
	aprendizaje?: string;
	
	pregunta?: string;
	
	tituloContenido?: string;
	URL?: string;
	tipoContenido?: string;

	tituloProyecto?: string;
	planteamientoProyecto?: string;
	investigacionProyecto?: string;
	planeacionProyecto?: string;
	creacionProyecto?: string;
	presentacionProyecto?: string;
}

export enum Componente {
	Tema, Aprendizaje, Pregunta, ContenidoDigital, Proyecto
}

export enum Operacion {
	Add, Edit
}