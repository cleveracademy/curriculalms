import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Http } from '@angular/http';

@Injectable()
export class BloqueResolve implements Resolve<any> {
    constructor(private http: Http) { }
    
    resolve(route: ActivatedRouteSnapshot): Promise<any> {
        return this.http.get('assets/bloque-messages.json').toPromise();
    }
}