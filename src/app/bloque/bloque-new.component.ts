import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { MdTab, MdTabGroup } from '@angular/material';
import { AngularFireAuth } from 'angularfire2/auth';
import { FirebaseListObservable } from 'angularfire2/database';
import { MateriaService } from './../materia/materia.service';
import { BloqueService, Bloque } from './bloque.service';
import { CleverKbBase } from './../cleverKB-base';
import { validate, IsNotEmpty } from 'class-validator';
import { MdDialog } from '@angular/material';
import { EditorElementosTema, ConfiguracionEditor, Componente, Operacion } from './editorElementosTema.component';

@Component({
  selector: 'app-bloque-new',
  templateUrl: './bloque-new.component.html'
})

export class BloqueNewComponent extends CleverKbBase implements OnInit {
  private bloque: Bloque;

  private temaTexto: string;
  private aprendizajeTexto: string;

  //Repositorio de los mensajes de error
  private messages: any;
  private message: string;

  //Almacenamiento de elementos seleccionados de las listas
  private temas: any;
  private aprendizajes: any;

  //Colecciones con valores leídos desde la base de datos
  private materias: FirebaseListObservable<any>;

	constructor(private materiaService: MateriaService, 
              private bloqueService: BloqueService, 
              private temaDialog: MdDialog,
              private auth: AngularFireAuth, 
              private location: Location, 
              private router: Router, 
              private route: ActivatedRoute,
              private http: Http) {
      super();
      super.validateUser(auth, router);
  	}

	ngOnInit() {
    this.bloque = new Bloque();
    this.route.data.subscribe(data => {
      this.messages = data.messages.json();
    });
		this.materiaService.getMaterias().subscribe(materias => this.materias = materias);
  }

	goBack(): void {
	    this.location.back();
	}

  onExtractTemas(): void {
    if (super.isNotEmpty(this.temaTexto)) {
      let lineas = this.temaTexto.split('\n');
      for (let linea of lineas) {
        if (super.isNotEmpty(linea))
          this.bloque.temas.push({tema: linea});
      }
      this.temaTexto = "";
    }
    else
     this.message = this.messages['errorAddTema'];
  }

  onRemoveTemas(): void {
    if (super.isNotEmpty(this.temas)) {
      for (let miTema of this.temas) {
        let result = confirm("Eliminar permanentemente el tema " + miTema['tema']);
        if (result)
          this.bloque.temas = this.bloque.temas.filter(tema => tema !== miTema);
      }
    }
    else
      this.message = this.messages['errorRemoveTema'];
  }

  onEditTema(): void {
    let configuracion = {operacion: Operacion.Edit, componente: Componente.Tema, tema: this.temas[0].tema };
    let dialogRef = this.temaDialog.open(EditorElementosTema, { data: configuracion});
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'add' && super.isNotEmpty(dialogRef.componentInstance.tema)) {
        this.temas[0].tema = dialogRef.componentInstance.tema;
      }
    });
  }

  onExtractAprendizajes(): void {
    if (super.isNotEmpty(this.aprendizajeTexto)) {
      let lineas = this.aprendizajeTexto.split('\n');
      for (let linea of lineas) {
        if (super.isNotEmpty(linea))
          this.bloque.aprendizajes.push({aprendizaje: linea});
      }
      this.aprendizajeTexto = "";
    }
    else
     this.message = this.messages['errorAddAprendizaije'];
  }

  onRemoveAprendizajes(): void {
    if (super.isNotEmpty(this.aprendizajes)) {
      for (let miAprendizaje of this.aprendizajes) {
        let result = confirm("Eliminar permanentemente el tema " + miAprendizaje['aprendizaje']);
        if (result)
          this.bloque.aprendizajes = this.bloque.aprendizajes.filter(aprendizaje => aprendizaje !== miAprendizaje);
      }
    }
    else
      this.message = this.messages['errorRemoveAprendizaje'];
  }

  onEditAprendizaje(): void {
    let configuracion = {operacion: Operacion.Edit, componente: Componente.Aprendizaje, aprendizaje: this.aprendizajes[0].aprendizaje };
    let dialogRef = this.temaDialog.open(EditorElementosTema, { data: configuracion});
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'add' && super.isNotEmpty(dialogRef.componentInstance.aprendizaje)) {
        this.aprendizajes[0].aprendizaje = dialogRef.componentInstance.aprendizaje;
      }
    });
  }

  onAdd(): void {
    validate(this).then(errors => {
      if (errors.length > 0) {
        this.message = super.extractErrorMessages(errors);
      }
      else {
        let bloqueId = this.bloqueService.add(this.bloque);
        this.router.navigate(['/bloqueEdit', bloqueId]);
      }
    });
  }

}
