import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { IsNotEmpty, IsNumber } from 'class-validator';

import { MateriaService } from './../materia/materia.service';
import { GradoService } from './../grado/grado.service';
import { NivelService } from './../nivel/nivel.service';

import { Observable, BehaviorSubject } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class BloqueService {
	private BLOQUES_TAG = "/bloques";
	private BLOQUE_TEMA_TAG = "/bloqueTemas";
	private BLOQUE_PROYECTO_TAG = "/bloqueProyectos";
	private BLOQUE_APRENDIZAJE_TAG = "/bloqueAprendizajes";

	private ELEMENTO_APROBADO = "aprobada";
	private ELEMENTO_PENDIENTE = "pendiente";

	constructor(private database: AngularFireDatabase, 
  				private materiaService: MateriaService, 
  				private gradoService: GradoService, 
  				private nivelService: NivelService) {
  	}
  	/**
  	 * Obtiene todos los bloques disponibles en la base
  	*/
  	getBloques(): Observable<Bloque[]> {
  		let myList: FirebaseListObservable<any>;

  		myList = this.database.list(this.BLOQUES_TAG, {query: {orderByChild: 'materia'}});
		
		//Para agregar texto de materia
		return myList.map(bloques => {
			return bloques.map(bloque => {
				this.materiaService.getMateria(bloque.materia).subscribe(materia => {
					bloque.txtMateria = materia.nombre;
					this.gradoService.getGrado(materia.grado).subscribe(grado => {
						bloque.txtGrado = grado.grado;
						this.nivelService.getNivel(grado.nivel).subscribe(nivel => {
							bloque.txtNivel = nivel.nivel;
						});
					});
				});
				return bloque;
			});
		});
	}

	getbloque(id: string): Observable<Bloque> {
		let bloque = this.database.object(this.BLOQUES_TAG + '/' + id);
		let temasDB = this.database.list(this.BLOQUE_TEMA_TAG, {query: { orderByChild: "bloque", equalTo: id }});
		let proyectosDB = this.database.list(this.BLOQUE_PROYECTO_TAG, {query: { orderByChild: "bloque", equalTo: id }});
		let aprendizajesDB = this.database.list(this.BLOQUE_APRENDIZAJE_TAG, {query: {orderByChild: "bloque", equalTo: id}});

		return bloque.map(bloque => {
			let b = new Bloque();
			b.idBloque = id;
			b.numero = bloque['numero'];
			b.nombre = bloque['nombre'];
			b.descripcion = bloque['descripcion'];
			b.idMateria = bloque['materia'];

			this.materiaService.getMateria(bloque.materia).subscribe(materia => {
				b.txtMateria = materia.nombre;
			});
			
			temasDB.subscribe(temas => b.temas = temas);

			aprendizajesDB.subscribe(aprendizajes => {
				b.aprendizajes = [];
				b.aprendizajesPorValidar = [];
				for (let a of aprendizajes) {
					if (a['status'] === this.ELEMENTO_APROBADO)
						b.aprendizajes.push(a);
					else
						b.aprendizajesPorValidar.push(a);
				}
			});

			proyectosDB.subscribe(proyectos => {
				b.proyectos = [];
				b.proyectosPorValidar = [];
				for (let p of proyectos) {
					if (p['status'] === this.ELEMENTO_APROBADO)
						b.proyectos.push(p);
					else
						b.proyectosPorValidar.push(p);
				}
			});
			return b;
		});
	}

	getTema(idTema: string): Observable<Tema> {
		return this.database.object(this.BLOQUE_TEMA_TAG + '/' + idTema);
	}

	updateBloqueTema(idTema: string, tema: string): void {
		this.database.object(this.BLOQUE_TEMA_TAG + '/' + idTema).update({ tema: tema });
	}

	update(bloque: Bloque): void {
		let bloqueDb = this.database.object(this.BLOQUES_TAG + '/' + bloque.idBloque);
		bloqueDb.update({
			numero: bloque.numero,
			nombre: bloque.nombre, 
			descripcion: bloque.descripcion, 
			materia: bloque.idMateria
		});
	}	

	delete(id: string): void {
		this.database.object(this.BLOQUES_TAG + '/' + id).remove();

		//Borrado de temas
		this.database.list(this.BLOQUE_TEMA_TAG, {query: { orderByChild: "bloque", equalTo: id }})
			.subscribe(temas => {
				for (let bloqueTema of temas) {
					this.database.object(this.BLOQUE_TEMA_TAG + '/' + bloqueTema.$key).remove();
				}
			});
		//Borrado de aprendizajes
		this.database.list(this.BLOQUE_APRENDIZAJE_TAG, {query: {orderByChild: "bloque", equalTo: id}})
			.subscribe(aprendizajes => {
				for (let ap of aprendizajes) {
					this.database.object(this.BLOQUE_APRENDIZAJE_TAG + "/" + ap.$key).remove();
				}
			})
	}

	/**
	 * Almacena los datos básicos de un bloque dentro de la base de datos
	 * @return Regresa como resultado el identificador del bloque recien creado
	 */
	add(bloque: Bloque) {
		let idBloque = this.database.list(this.BLOQUES_TAG).push({ 
		   numero: bloque.numero,
		   nombre: bloque.nombre, 
		   materia: bloque.idMateria, 
		   descripcion: bloque.descripcion
		});
		
		for (let tema of bloque.temas) {
			this.addBloqueTema(idBloque.key, tema.tema);
		}

		for (let aprendizaje of bloque.aprendizajes) {
			this.addBloqueAprendizaje(idBloque.key, aprendizaje);
		}

		return idBloque.key;
	}

	addBloqueAprendizaje(idBloque: string, aprendizaje: Aprendizaje) : void {
		this.database.list(this.BLOQUE_APRENDIZAJE_TAG).push({
			bloque: idBloque,
			aprendizaje: aprendizaje.aprendizaje,
			status: this.ELEMENTO_APROBADO
		});
	}

	deleteBloqueAprendizaje(idBloqueAprendizaje: string): void {
		this.database.list(this.BLOQUE_APRENDIZAJE_TAG + "/" + idBloqueAprendizaje).remove();
	}

	updateBloqueAprendizaje(idBloqueAprendizaje: string, aprendizaje: Aprendizaje): void {
		this.database.object(this.BLOQUE_APRENDIZAJE_TAG + '/' + idBloqueAprendizaje).update({
			aprendizaje: aprendizaje.aprendizaje,
		});
	}

	addBloqueTema(idBloque: string, tema: string): void {
		this.database.list(this.BLOQUE_TEMA_TAG).push({
			tema: tema,
			bloque: idBloque
		});
	}

	deleteBloqueTema(idBloqueTema: string): void {
		this.database.list(this.BLOQUE_TEMA_TAG + '/' + idBloqueTema).remove();
	}

	addBloqueProyecto(idBloque: string, proyecto: Proyecto): void {
		this.database.list(this.BLOQUE_PROYECTO_TAG).push({
			titulo: proyecto.titulo,
			planteamiento: proyecto.planteamiento,
			investigacion: proyecto.investigacion, 
			planeacion: proyecto.planeacion, 
			creacion: proyecto.creacion, 
			presentacion: proyecto.presentacion,
			status: this.ELEMENTO_APROBADO,
			bloque: idBloque
		});
	}

	deleteBloqueProyecto(idBloqueProyecto: string): void {
		this.database.object(this.BLOQUE_PROYECTO_TAG + '/' + idBloqueProyecto).remove();
	}

	updateBloqueProyecto(idBloqueProyecto: string, proyecto: Proyecto): void {
		this.database.object(this.BLOQUE_PROYECTO_TAG + '/' + idBloqueProyecto).update({
			titulo: proyecto.titulo,
			planteamiento: proyecto.planteamiento,
			investigacion: proyecto.investigacion, 
			planeacion: proyecto.planeacion, 
			creacion: proyecto.creacion, 
			presentacion: proyecto.presentacion
		});
	}

	validateBloqueProyecto(idBloqueProyecto: string): void {
		this.database.object(this.BLOQUE_PROYECTO_TAG + '/' + idBloqueProyecto).update({status: this.ELEMENTO_APROBADO});
	}

	validateBloqueAprendizaje(idBloqueAprendizaje: string): void {
		this.database.object(this.BLOQUE_APRENDIZAJE_TAG + '/' + idBloqueAprendizaje).update({status: this.ELEMENTO_APROBADO});
	}
}
	//////////////////////////////////////////// CLASES ADICIONALES ////////////////////////////////////////////

export class Bloque {
	//@IsNotEmpty({ message: "Número no puede estar vacío"})
	//@IsNumber({message: "Número no tiene un valor válido"})
  	numero: number;
  	@IsNotEmpty({ message: "Nombre no puede estar vacío"})
	nombre: string;
  	@IsNotEmpty({ message: "Descripcion no puede estar vacío"})
  	descripcion: string;

	idBloque: string;
	idMateria: number;
	txtMateria: string;

	temas?: Array<Tema>;

	proyectos?: Array<Proyecto>;
	proyectosPorValidar?: Array<Proyecto>;

	aprendizajes?: Array<Aprendizaje>;
	aprendizajesPorValidar?: Array<Aprendizaje>;

	constructor() {
		this.nombre = "";
		this.temas = new Array();
		this.aprendizajes = new Array();
	}
}

export class Tema {
	tema: string;
}

export class Aprendizaje {
	aprendizaje: string;
	/** Por validar o aprovado */
	status?: string;
}

export class Proyecto {
	@IsNotEmpty({message: "El título no puede estar vacío"})
	titulo: string;
	@IsNotEmpty({message: "El planteamiento no puede estar vacío"})
	planteamiento: string;
	@IsNotEmpty({message: "La investigación no puede estar vacía"})
	investigacion: string;
	@IsNotEmpty({message: "La planeación no puede estar vacía"})
	planeacion: string;
	@IsNotEmpty({message: "La creación no puede estar vacía"})
	creacion: string;
	@IsNotEmpty({message: "La presentación no puede estar vacía"})
	presentacion: string;

	constructor() {
		this.titulo = "";
		this.planteamiento = "";
		this.investigacion = "";
		this.planeacion = "";
		this.creacion = "";
		this.presentacion = "";		
	}
}
