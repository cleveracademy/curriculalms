import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { Observable } from 'rxjs';
import { IsNotEmpty, IsUrl } from 'class-validator';

import { BloqueService } from './bloque.service';

/* Tipo de contenido digital */
export const TipoContenidoDigital = [{nombre: "Lectura"}, {nombre: "Juego"}, {nombre: "Proyecto"}, {nombre: "Video"}, {nombre: "Otro"}];

@Injectable()
export class TemaService {
	private TEMA_PREGUNTA_TAG = "/temaPreguntas";
	private TEMA_CONTENIDO_TAG = "/temaContenidos";

	private ELEMENTO_APROBADO = "aprobada";
	private ELEMENTO_PENDIENTE = "pendiente";

	constructor(private database: AngularFireDatabase, 
				private bloqueService: BloqueService) { }

	getTema(idTema: string): Observable<Tema> {
		let temaDB = this.bloqueService.getTema(idTema);
		let preguntasDB = this.database.list(this.TEMA_PREGUNTA_TAG, {query: { orderByChild: "tema", equalTo: idTema }});
		let contenidosDB = this.database.list(this.TEMA_CONTENIDO_TAG, {query: { orderByChild: "tema", equalTo: idTema }});

		return temaDB.map(datos => {
			let tema = new Tema();
			tema.tema = datos.tema;
			tema.idTema = idTema;
			tema.idBloque = datos['bloque'];
			
			preguntasDB.subscribe(preguntas => {
				tema.preguntas = [];
				tema.preguntasPorValidar = [];
				for (let p of preguntas) {
					if (p['status'] === this.ELEMENTO_APROBADO)
						tema.preguntas.push(p);
					else
						tema.preguntasPorValidar.push(p);
				}
			});

			contenidosDB.subscribe(contenidos => {
				tema.contenidos = [];
				tema.contenidosPorValidar = [];
				for (let c of contenidos) {
					if (c['status'] === this.ELEMENTO_APROBADO)
						tema.contenidos.push(c);
					else
						tema.contenidosPorValidar.push(c);
				}
			});

			return tema;
		});
	}

	addTemaPregunta(idTema: string, pregunta: string): void {
		this.database.list(this.TEMA_PREGUNTA_TAG).push({
			pregunta: pregunta,
			status: this.ELEMENTO_APROBADO,
			tema: idTema
		});
	}

	addTemaContenido(idTema: string, contenido: Contenido): void {
		this.database.list(this.TEMA_CONTENIDO_TAG).push({
			titulo: contenido.titulo,
			URL: contenido.URL,
			tipo: contenido.tipo,
			status: this.ELEMENTO_APROBADO,
			tema: idTema
		});
	}

	deleteTemaPregunta(idTemaPregunta: string): void {
		this.database.object(this.TEMA_PREGUNTA_TAG + '/' + idTemaPregunta).remove();
	}

	deleteTemaContenido(idTemaContenido: string): void {
		this.database.object(this.TEMA_CONTENIDO_TAG + '/' + idTemaContenido).remove();
	}

	updateTemaPregunta(idTemaPregunta: string, pregunta: string): void {
		this.database.object(this.TEMA_PREGUNTA_TAG + '/' + idTemaPregunta).update({
			pregunta: pregunta,
		});
	}

	updateTemaContenido(idTemaContenido: string, contenido: Contenido): void {
		this.database.object(this.TEMA_CONTENIDO_TAG + '/' + idTemaContenido).update({
			titulo: contenido.titulo,
			URL: contenido.URL,
			tipo: contenido.tipo
		});
	}

	validateTemaPregunta(idTemaPregunta: string): void {
		this.database.object(this.TEMA_PREGUNTA_TAG + '/' + idTemaPregunta).update({status: this.ELEMENTO_APROBADO});
	}

	validateTemaContenido(idTemaContenido: string): void {
		this.database.object(this.TEMA_CONTENIDO_TAG + '/' + idTemaContenido).update({status: this.ELEMENTO_APROBADO});
	}
}

//////////////////////////////////////////// CLASES ADICIONALES ////////////////////////////////////////////

export class Tema {
	idBloque: string;
	idTema: string;
	tema: string;

	preguntas?: Array<string>;
	contenidos?: Array<Contenido>;

	preguntasPorValidar?: Array<string>;
	contenidosPorValidar?: Array<Contenido>;

	constructor() {
		this.tema = "";
	}
}

export class Contenido {
	@IsNotEmpty({message: "El título no puede estar vacío"})
	titulo: string;
	@IsNotEmpty({message: "La URL no puede estar vacía"})
	@IsUrl()
	URL: string;
	tipo: string;

	constructor() {
		this.titulo = "";
		this.URL = "";
		this.tipo = "";
	}
}