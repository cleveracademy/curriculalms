import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { CleverKbBase } from './../cleverKB-base';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { ValidationError, validate, IsNotEmpty } from 'class-validator';

import { BloqueService, Bloque } from './bloque.service';
import { MateriaService } from './../materia/materia.service';
import { MyDataSource } from './../myDataSource';
import { MdSort, MdSelect } from '@angular/material';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-bloque',
  templateUrl: './bloque.component.html',
  styleUrls: ['bloque.component.css']
})

export class BloqueComponent extends CleverKbBase implements OnInit {
	title = "Bloques";
	//Las columnas deben llevar los nombres de los atributos que serán utilizados para ordenamiento
	displayedColumns = ['numero', 'nombre', 'txtMateria', 'txtGrado', 'txtNivel', 'edit', 'delete', 'details'];
	//Los campos de búsqueda son los nombres de los atributos que serán utilizados para filtrado
	searchColumns = ['nombre', 'txtMateria', 'txtGrado', 'txtNivel'];
  	dataSource: MyDataSource<Bloque>;
  	
  	//Elementos para filtrado
  	materias: Observable<any>;
	materia: any;

  	/* Finds the first MdSort element in the HTML and obtains its native reference inside the DOM */
  	@ViewChild(MdSort) sort: MdSort;
  	/* Finds the element called 'filter' in the HTML and obtains its native reference inside the DOM */
  	@ViewChild('filter') filter: ElementRef;

	constructor(private auth: AngularFireAuth, private router: Router, 
		private bloqueService: BloqueService, private materiaService: MateriaService) {
  		super();
    	super.validateUser(auth, router);
  	}

	ngOnInit() {
		this.dataSource = new MyDataSource(this.bloqueService.getBloques(), this.sort, this.filter, this.searchColumns);
		this.materiaService.getMaterias().subscribe(materias => this.materias = materias);
	}

	onAdd(): void {
		this.router.navigate(['/bloqueNew']);
	}

	onEdit(bloqueID: string): void {
		this.router.navigate(['/bloqueEdit', bloqueID]);
	}

	onDelete(bloque: any): void {
		let result = confirm("Eliminar permanentemente el bloque " + bloque['nombre']);
  		if (result)
      		this.bloqueService.delete(bloque.$key);
	}

	onDetails(bloqueID: string): void {
		this.router.navigate(['/bloqueDetail', bloqueID]);
	}

	onMateriaChanged(): void {
		if (this.materia)
			this.dataSource.filter = this.materia.nombre + " " + this.materia.txtNivel + " " + this.materia.txtGrado;
		else
			this.dataSource.filter = "";
	}
}