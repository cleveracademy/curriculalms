import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { validate, IsNotEmpty } from 'class-validator';
import { MateriaService } from './../materia/materia.service';
import { GrupoService } from './grupo.service';
import { CleverKbBase } from './../cleverKB-base';
@Component({
  selector: 'app-grupo-edit',
  templateUrl: './grupo-edit.component.html'
})

export class GrupoEditComponent extends CleverKbBase implements OnInit {
	grupo: FirebaseObjectObservable<any>;
	materias: FirebaseListObservable<any>;
	
  message: string;
 	
  @IsNotEmpty({ message: "Nombre no puede estar vacío"})
 	private txtGrupo: string;
  @IsNotEmpty({ message: "Selecciona una materia"})
   private txtMateria: string;

	constructor(private materiaService: MateriaService, 
				private grupoService: GrupoService,
	          	private route: ActivatedRoute, 
	          	private location: Location,
	          	private auth: AngularFireAuth,
	          	private router: Router) {

		    super();
    super.validateUser(auth, router);
	}

	ngOnInit() {
		this.materiaService.getMaterias().subscribe(materias => this.materias = materias);
    	this.route.params.subscribe((params: Params) => {
      		this.grupoService.getgrupo(params['id']).subscribe(grupo => {
        	this.grupo = grupo;
      	});
    });
	}

	goBack(): void {
		this.location.back();
	}

  onUpdate(id: string): void {
    this.txtGrupo = this.grupo['nombre'];
    this.txtMateria = this.grupo['materia'];
    validate(this).then(errors => {
      if (errors.length > 0) {
        this.message = super.extractErrorMessages(errors);
      }
      else {
        this.grupoService.updateGrupo(id, this.grupo);
        this.router.navigate(['/grupos']);
      }
    });
  }

}
