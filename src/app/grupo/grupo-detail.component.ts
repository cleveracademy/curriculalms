import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { MateriaService } from './../materia/materia.service';
import { GrupoService } from './grupo.service';

@Component({
  selector: 'app-grupo-detail',
  templateUrl: './grupo-detail.component.html'
})
export class GrupoDetailComponent implements OnInit {
	grupo: FirebaseObjectObservable<any>;
	materias: FirebaseListObservable<any>;


	constructor(private materiaService: MateriaService, 
				private grupoService: GrupoService,
	          	private route: ActivatedRoute, 
	          	private location: Location,
	          	private router: Router) {}

	ngOnInit() {
		this.materiaService.getMaterias().subscribe(materias => this.materias = materias);
    	this.route.params.subscribe((params: Params) => {
      		this.grupoService.getgrupo(params['id']).subscribe(grupo => {
        	this.grupo = grupo;
      	});
    });
	}

	goBack(): void {
		this.location.back();
	}


}
