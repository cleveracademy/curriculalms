import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';

import { MateriaService } from './../materia/materia.service';

import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class GrupoService {
	private grupoS_TAG = "/grupos";

  	constructor(private database: AngularFireDatabase, private materiaService: MateriaService) {}

  	getgrupos(): Observable<any> {
		return this.database.list(this.grupoS_TAG).map(grupos => {
			return grupos.map(grupo => {
				this.materiaService.getMateria(grupo.materia).subscribe(materia => {
					grupo.txtMateria = materia.nombre;
				});
				
				return grupo;
			});
			
		});
	}

	getgrupo(id: string): Observable<any> {
		return this.database.object(this.grupoS_TAG + '/' + id).map(grupo => {
			this.materiaService.getMateria(grupo.materia).subscribe(materia => {
				grupo.txtMateria = materia.nombre;
			});
			return grupo;
		});
	}

	updateGrupo(id: string, grupo: FirebaseObjectObservable<any>): void {
		this.database.object(this.grupoS_TAG + '/' + id).update({nombre: grupo['nombre'], 
								  materia: grupo['materia']});
	}

	delete(id: string): void {
		this.database.object(this.grupoS_TAG + '/' + id).remove();
	}

	addGrupo(nombre: string, materia: string): void {
		this.database.list(this.grupoS_TAG).push({ 
													   nombre: nombre, 
													   materia: materia});
	}
}
