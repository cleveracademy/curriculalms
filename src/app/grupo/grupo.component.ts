import { Component, OnInit } from '@angular/core';
import { CleverKbBase } from './../cleverKB-base';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { ValidationError, validate, IsNotEmpty } from 'class-validator';
import { Observable } from 'rxjs';

import { GrupoService } from './grupo.service';
import { MateriaService } from './../materia/materia.service';

@Component({
  selector: 'app-grupo',
  templateUrl: './grupo.component.html'
})
export class GrupoComponent extends CleverKbBase implements OnInit {
	title = "grupos";
	grupos: Observable<any>;

	constructor(private auth: AngularFireAuth, 
				private router: Router, 
				private grupoService: GrupoService, private materiaService: MateriaService) {
  		super();
    	super.validateUser(auth, router);
  	}

	ngOnInit() {
		this.grupoService.getgrupos().subscribe(grupos => {
			for (let grupo of grupos) {
				this.materiaService.getMateria(grupo.materia).subscribe(materia => {
					grupo.txtGrado = materia.txtGrado;
					grupo.txtNivel = materia.txtNivel;
				});
			}
			this.grupos = grupos;
		});
	}

	onAdd(): void {
		this.router.navigate(['/grupoNew']);
	}

	onEdit(grupoID: string): void {
		this.router.navigate(['/grupoEdit', grupoID]);
	}

	onDelete(grupo: any): void {
		let result = confirm("Eliminar permanentemente el grupo " + grupo['nombre']);
  		if (result)
      	this.grupoService.delete(grupo.$key);
	}
	onDetail(id: string): void {
    this.router.navigate(['/grupoDetail', id]);
  }
}
