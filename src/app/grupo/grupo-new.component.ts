import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { MateriaService } from './../materia/materia.service';
import { GrupoService } from './grupo.service';
import { CleverKbBase } from './../cleverKB-base';
import { validate, IsNotEmpty } from 'class-validator';

@Component({
  selector: 'app-grupo-new',
  templateUrl: './grupo-new.component.html'
})
export class GrupoNewComponent extends CleverKbBase implements OnInit {
  @IsNotEmpty({ message: "Nombre no puede estar vacío"})
	nombre: string;
  @IsNotEmpty({ message: "Selecciona una materia"})
	materia: string;
  message: string;
 	materias: FirebaseListObservable<any>;


	constructor(private materiaService: MateriaService, 
              private grupoService: GrupoService,
              private location: Location, 
              private router: Router, 
              private auth: AngularFireAuth) {
    super();
    super.validateUser(auth, router);
  	}

	ngOnInit() {
		this.materiaService.getMaterias().subscribe(materias => this.materias = materias);
    	
	}

	goBack(): void {
	    this.location.back();
  	}


  onAdd(): void {
    validate(this).then(errors => {
      if (errors.length > 0) {
        this.message = super.extractErrorMessages(errors);
      } else {
        this.grupoService.addGrupo(this.nombre, this.materia);
        this.router.navigate(['/grupos']);
      }
    });
  }

}
