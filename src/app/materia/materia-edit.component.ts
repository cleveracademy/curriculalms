import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { CleverKbBase } from './../cleverKB-base';
import { GradoService } from './../grado/grado.service';
import { MateriaService } from './materia.service';
import { validate, IsNotEmpty } from 'class-validator';

@Component({
  selector: 'app-materia-edit',
  templateUrl: './materia-edit.component.html'
})

export class MateriaEditComponent extends CleverKbBase implements OnInit {
	materia: FirebaseObjectObservable<any>;
	grados: FirebaseListObservable<any>;
	idiomas = [{nombre: "Español"}, {nombre: "Inglés"}, {nombre: "Otro"}];

  enfoque: string;
  //Campo de texto para agregar elemento a una lista
  competeSEPTexto: string;
  competeTranTexto: string;
  
  messages: string;
  message: string;

  //Lista con todos los elementos
  competesSEP: any;
  competesTrans: any;

  listaCompeteSEP: any;
  listaCompeteTrans: any;

  //@IsNotEmpty({ message: "Clave no puede estar vacío"})
  private txtClave: string;
  @IsNotEmpty({ message: "Nombre no puede estar vacío"})
  private txtNombre: string;
  @IsNotEmpty({ message: "Selecciona un grado"})
  private txtGrado: string;
  @IsNotEmpty({ message: "Selecciona un idioma"})
  private txtIdioma: string;

	constructor(private gradoService: GradoService, 
				      private materiaService: MateriaService,
	          	private route: ActivatedRoute, 
	          	private location: Location, 
	          	private router: Router,
              private http: Http,
	          	private auth: AngularFireAuth) {
    super();
    super.validateUser(auth, router);
	}

	ngOnInit() {
  	this.gradoService.getGrados().subscribe(grados => this.grados = grados);

  	this.route.params.subscribe((params: Params) => {
  		this.materiaService.getMateria(params['id']).subscribe(materia => {
    	  this.materia = materia;
    	});
    });

    this.http.get('assets/materia-messages.json').subscribe(data => this.messages = data.json());
	}

	goBack(): void {
		this.location.back();
	}

      // para agregar a la lista de Competencias SEP
    onExtractCompeteSEPs(id: string): void {
    if (super.isNotEmpty(this.competeSEPTexto)) {
      let texto = this.competeSEPTexto.split('\n');
      for (let s of texto) {
        if (super.isNotEmpty(s))
            this.materiaService.addCompetenciaSEP(id, s);
      }
      this.competeSEPTexto = "";
    }
    else
      this.message = this.messages['errorAddcompeteSEP'];
  }

  onRemoveCompeteSEP(): void {
    if (super.isNotEmpty(this.competesSEP)) {
      for (let miCompetencia of this.competesSEP) {
        let result = confirm("Eliminar permanentemente la competencia " + miCompetencia['competencia']);
        if (result)
          this.materiaService.deleteCompetenciaSEP(miCompetencia.$key);
      }
    }
    else
      this.message = this.messages['errorRemoveCompeteSEP'];
  }

      // para agregar a la lista de Competencias Transversales

    onExtractCompeteTrans(id: string): void {
    if (super.isNotEmpty(this.competeTranTexto)) {
      let texto = this.competeTranTexto.split('\n');
      for (let t of texto) {
        if (super.isNotEmpty(t))
            this.materiaService.addCompetenciaTrans(id, t);
      }
      this.competeTranTexto = "";
    }
    else
      this.message = this.messages['errorAddCompeteTran'];
  }

  onRemoveCompeteTrans(): void {
    if (super.isNotEmpty(this.competesTrans)) {
      for (let miCompetencia of this.competesTrans) {
        let result = confirm("Eliminar permanentemente la competencia " + miCompetencia['competencia']);
        if (result)
          this.materiaService.deleteCompetenciaTrans(miCompetencia.$key);
      }
    }
    else
      this.message = this.messages['errorRemoveCompeteTrans'];
  }
  onUpdate(id: string): void {
    this.txtClave = this.materia['clave'];
    this.txtNombre = this.materia['nombre'];
    this.txtGrado = this.materia['grado'];
    this.txtIdioma = this.materia['idioma'];
    validate(this).then(errors => {
      if (errors.length > 0) {
        this.message = super.extractErrorMessages(errors);
      }
      else {
        this.materiaService.updateMateria(id, this.materia);
        this.router.navigate(['/materias']);
      }
    });
  }


}
