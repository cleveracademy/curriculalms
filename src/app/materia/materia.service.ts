import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { IsNotEmpty } from 'class-validator';

import { NivelService } from './../nivel/nivel.service';
import { GradoService } from './../grado/grado.service';

import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class MateriaService {
	private MATERIAS_TAG = "/materias";
	private MATERIA_COMPETENCIA_SEP = "/materiaCompetenciasSEP";
	private MATERIA_COMPETENCIA_TRANSVERSAL = "/materiaCompetenciasTrans";

  	constructor(private database: AngularFireDatabase,
  				private nivelService: NivelService,
  				private gradoService: GradoService) {}

  	getMaterias(): Observable<any> {
		return this.database.list(this.MATERIAS_TAG, {query: {orderByChild: 'nombre'}}).map(materias => {
			return materias.map(materia => {
				this.gradoService.getGrado(materia.grado).subscribe(grado => {
					materia.txtGrado = grado.grado;
					this.nivelService.getNivel(grado.nivel).subscribe(nivel => {
						materia.txtNivel = nivel.nivel;
					});
				});
				return materia;
			});

		});
	}
	getMateria(id: string): Observable<any> {
		let materia = this.database.object(this.MATERIAS_TAG + '/' + id);
		let listaCompetenciasSEP = this.database.list(this.MATERIA_COMPETENCIA_SEP, {query: {orderByChild: 'materia', equalTo: id}});
		let listaCompetenciasTrans = this.database.list(this.MATERIA_COMPETENCIA_TRANSVERSAL, {query: {orderByChild: 'materia', equalTo: id}});

		return materia.map(m => {
			listaCompetenciasSEP.subscribe(competencias => {
				m.listaCompeteSEP = competencias;
			});
			listaCompetenciasTrans.subscribe(competencias => m.listaCompeteTrans = competencias);
			this.gradoService.getGrado(m.grado).subscribe(grado => {
				m.txtGrado = grado.grado;
				this.nivelService.getNivel(grado.nivel).subscribe(nivel => {
					m.txtNivel = nivel.nivel;
				});
			});
			return m;
		});


	}
/*
	getMateria(id: string): Observable<Materia> {
		let materia = this.database.object(this.MATERIAS_TAG + '/' + id);
		let listaCompetenciasSEP = this.database.list(this.MATERIA_COMPETENCIA_SEP, {query: {orderByChild: 'materia', equalTo: id}});
		let listaCompetenciasTrans = this.database.list(this.MATERIA_COMPETENCIA_TRANSVERSAL, {query: {orderByChild: 'materia', equalTo: id}});

		return materia.map(materia => {
			let m = new Materia();
			listaCompetenciasSEP.subscribe(competencias => {
				materia.listaCompeteSEP = competencias;
			});
			listaCompetenciasTrans.subscribe(competencias => materia.listaCompeteTrans = competencias);
			this.gradoService.getGrado(materia.grado).subscribe(grado => {
				materia.txtGrado = grado.grado;
				this.nivelService.getNivel(grado.nivel).subscribe(nivel => {
					materia.txtNivel = nivel.nivel;
				});
			});
			return m;
		});

	*/


	updateMateria(id: string, materia: FirebaseObjectObservable<any>): void {
		let materiaRef = this.database.object(this.MATERIAS_TAG + '/' + id);
		materiaRef.update({
			clave: materia['clave'],
			nombre: materia['nombre'],
			grado: materia['grado'],
			idioma: materia['idioma'],
			esOficial: materia['esOficial'],
			enfoque: materia['enfoque']
		});
	}


	delete(id: string): void {
		let materiaRef = this.database.object(this.MATERIAS_TAG + '/' + id);
		materiaRef.remove();
	}

	addMateria(clave: string,
				nombre: string,
				grado: string,
				idioma: string,
				esOficial: boolean,
				enfoque: string,
				competeSEP: Array<any>,
				competeTrans: Array<any>): void {
		clave = (clave == null) ? '': clave;
		esOficial = esOficial != null;
		enfoque = (enfoque == null) ? '' : enfoque;

		let materiaID = this.database.list(this.MATERIAS_TAG).push({
			clave: clave,
		   	nombre: nombre,
		 	grado: grado,
		  	idioma: idioma,
		   	esOficial: esOficial,
		   	enfoque: enfoque});

		let listaCompetenciasSEP = this.database.list(this.MATERIA_COMPETENCIA_SEP);

		for (let c of competeSEP) {
			listaCompetenciasSEP.push({
				competencia: c.competencia,
				materia: materiaID.key
			});
		}

		let listaCompetenciasTrans = this.database.list(this.MATERIA_COMPETENCIA_TRANSVERSAL);

		for (let c of competeTrans) {
			listaCompetenciasTrans.push({
				competencia: c.competencia,
				materia: materiaID.key
			});
		}

	}

	addCompetenciaSEP(materiaId: string, competencia: string): void {
		let listaCompetenciasSEP = this.database.list(this.MATERIA_COMPETENCIA_SEP);
		listaCompetenciasSEP.push({
			competencia: competencia,
			materia: materiaId
		});
	}

	deleteCompetenciaSEP(competenciaId: string): void {
		this.database.object(this.MATERIA_COMPETENCIA_SEP + '/' + competenciaId).remove();
	}


	addCompetenciaTrans(materiaId: string, competencia: string): void {
		let listaCompetenciasTrans = this.database.list(this.MATERIA_COMPETENCIA_TRANSVERSAL);
		listaCompetenciasTrans.push({
			competencia: competencia,
			materia: materiaId
		});
	}

	deleteCompetenciaTrans(competenciaId: string): void {
		this.database.object(this.MATERIA_COMPETENCIA_TRANSVERSAL + '/' + competenciaId).remove();
	}

	getGrados(): Observable<any> {
		return this.gradoService.getGrados();
	}


}
	//////////////////////////////////////////// CLASES ADICIONALES ////////////////////////////////////////////

export class Materia {
	private clave: string;
	@IsNotEmpty({ message: "Nombre no puede estar vacío"})
	private nombre: string;
	@IsNotEmpty({ message: "Selecciona un grado"})
	private txtGrado: string;
	@IsNotEmpty({ message: "Selecciona un idioma"})
	private txtIdioma: string;
	private esOficial: boolean;
	private enforque?: string;

	private competenciasSep?: Array<any>;
	private competenciasTans?: Array<any>;


	constructor() {
		this.clave = "";
		this.nombre = "";
		this. txtGrado = "";
		this.txtIdioma = "";
		this.esOficial = false;
	}
}

export class Competencia {
	competencia: string;
	materiaId: string;
}
