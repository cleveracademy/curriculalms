import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { AngularFireAuth } from 'angularfire2/auth';
import { GradoService } from './../grado/grado.service';
import { MateriaService } from './materia.service';
import { CleverKbBase } from './../cleverKB-base';
import { validate, IsNotEmpty } from 'class-validator';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-materia-new',
  templateUrl: './materia-new.component.html'
})
export class MateriaNewComponent extends CleverKbBase implements OnInit {
	//@IsNotEmpty({ message: "Clave no puede estar vacío"})
  clave: string;
  @IsNotEmpty({ message: "Nombre no puede estar vacío"})
	nombre: string;
  @IsNotEmpty({ message: "Selecciona un grado"})
	grado: any;
  @IsNotEmpty({ message: "Selecciona un idioma"})
	idioma: string;
  esOficial: boolean;

  enfoque: string;
  //Campo de texto para agregar elemento a una lista
  competeSEPTexto: string;
  competeTranTexto: string;
	
  message: string;

  //Lista con los elementos seleccionados
  competesSEP: string;
  competesTrans: string;

  messages: any;

  //Lista con todos los elementos
  listaCompeteSEP = [];
  listaCompeteTrans = [];

  niveles: Observable<any>;
  grados: Observable<any>;
  idiomas = [{nombre: "Español"}, {nombre: "Inglés"}, {nombre: "Otro"}];



	constructor(private gradoService: GradoService, 
              private materiaService: MateriaService,
              private location: Location, 
              private router: Router,
              private http: Http, 
              private auth: AngularFireAuth) {
	super();
    super.validateUser(auth, router);
  }

	ngOnInit() {
    this.gradoService.getGrados().subscribe(grados => this.grados = grados);
    this.http.get('assets/materia-messages.json').subscribe(data => this.messages = data.json());

	}

	goBack(): void {
	    this.location.back();
  	}
      // para agregar a la lista de Competencias SEP
    onExtractCompeteSEPs(id: string): void {
    if (super.isNotEmpty(this.competeSEPTexto)) {
      let texto = this.competeSEPTexto.split('\n');
      for (let s of texto) {
        if (super.isNotEmpty(s))
          this.listaCompeteSEP.push({competencia: s});
      }
      this.competeSEPTexto = "";
    }
    else
      this.message = this.messages['errorAddcompeteSEP'];
  }



  // para remover de la lista de Competencias SEP
  onDeleteCompeteSEP(): void {
    if (super.isNotEmpty(this.competesSEP)) {
      for (let miCompeteSEP of this.competesSEP) {
        let result = confirm("Eliminar permanentemente la competencia " + miCompeteSEP['competencia']);
        if (result)
          this.listaCompeteSEP = this.listaCompeteSEP.filter(competeSEP => competeSEP !== miCompeteSEP);
      }
    }
    else
      this.message = this.messages['errorRemoveCompeteSEP'];
  }

      // para agregar a la lista de Competencias transversales
    onExtractCompeteTrans(id: string): void {
    if (super.isNotEmpty(this.competeTranTexto)) {
      let texto = this.competeTranTexto.split('\n');
      for (let t of texto) {
        if (super.isNotEmpty(t))
            this.listaCompeteTrans.push({competencia: t});
      }
      this.competeTranTexto = "";
    }
    else
      this.message = this.messages['errorAddCompeteTran'];
  }

  // para remover de la lista de Competencias SEP
  onDeleteCompeteTrans(): void {
    if (super.isNotEmpty(this.competesTrans)) {
      for (let miCompeteTrans of this.competesTrans) {
        let result = confirm("Eliminar permanentemente la competencia " + miCompeteTrans['competencia']);
        if (result)
          this.listaCompeteTrans = this.listaCompeteTrans.filter(competeTrans => competeTrans !== miCompeteTrans);
      }
    }
    else
      this.message = this.messages['errorRemoveCompeteTrans'];
  }

  onAdd(): void {
    validate(this).then(errors => {
      if (errors.length > 0) {
        this.message = super.extractErrorMessages(errors);
      } else {
        this.materiaService.addMateria(this.clave, 
          this.nombre, 
          this.grado, 
          this.idioma, 
          this.esOficial, 
          this.enfoque, 
          this.listaCompeteSEP, 
          this.listaCompeteTrans);
        this.router.navigate(['/materias']);
      }
    });
  }

  onSelectGrado(): void {
    if (!super.isNotEmpty(this.clave) && super.isNotEmpty(this.nombre) && super.isNotEmpty(this.grado)) {
      let grado = this.grados.filter((grado, index) => grado.$key == this.grado)[0];
      this.clave = this.nombre.substring(0, 2) + grado.grado.substring(0,2) + grado.txtNivel.substring(0,2);
      this.clave = this.clave.toLowerCase() + Math.round(Math.random() * 100);
    }
  }
}