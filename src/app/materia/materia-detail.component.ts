import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { GradoService } from './../grado/grado.service';
import { MateriaService } from './materia.service';

@Component({
  selector: 'app-materia-detail',
  templateUrl: './materia-detail.component.html'
})
export class MateriaDetailComponent implements OnInit {
	materia: FirebaseObjectObservable<any>;
	grados: FirebaseListObservable<any>;
	idiomas = [{nombre: "Español"}, {nombre: "Inglés"}, {nombre: "Otro"}];

	constructor(private gradoService: GradoService, 
				private materiaService: MateriaService,
	          	private route: ActivatedRoute, 
	          	private location: Location, 
	          	private router: Router) {}

	ngOnInit() {
    	this.gradoService.getGrados().subscribe(grados => this.grados = grados);
    	this.route.params.subscribe((params: Params) => {
      		this.materiaService.getMateria(params['id']).subscribe(materia => {
        	this.materia = materia;
      	});
    });
	}

	goBack(): void {
		this.location.back();
	}


}
