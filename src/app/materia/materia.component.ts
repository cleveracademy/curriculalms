import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { CleverKbBase } from './../cleverKB-base';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { ValidationError, validate, IsNotEmpty } from 'class-validator';
import { Observable } from 'rxjs';

import { MateriaService, Materia } from './materia.service';
import { MyDataSource } from './../myDataSource';
import { MdSort, MdSelect } from '@angular/material';

@Component({
  selector: 'app-materia',
  templateUrl: './materia.component.html'
})
export class MateriaComponent extends CleverKbBase implements OnInit {
	title = "Materias";

  displayedColumns = ['clave', 'nombre', 'txtGrado', 'txtNivel', 'idioma', 'oficial', 'edit', 'delete', 'details'];
  searchColumns = ['nombre', 'txtGrado', 'txtNivel', 'idioma'];
  dataSource: MyDataSource<any>;

	grados: Observable<any>;
  grado: any;

  /* Finds the first MdSort element in the HTML and obtains its native reference inside the DOM */
  @ViewChild(MdSort) sort: MdSort;
  /* Finds the element called 'filter' in the HTML and obtains its native reference inside the DOM */
  @ViewChild('filter') filter: ElementRef;

	constructor(private auth: AngularFireAuth,
				private router: Router,
				private materiaService: MateriaService) {
  		super();
    	super.validateUser(auth, router);
  	}

	ngOnInit() {

    this.materiaService.getGrados().subscribe(grados => {
			this.grados = grados;
		});
    this.dataSource = new MyDataSource(this.materiaService.getMaterias(), this.sort, this.filter, this.searchColumns);

	}

	onAdd(): void {
		this.router.navigate(['/materiaNew']);
	}

	onEdit(materiaID: string): void {
		this.router.navigate(['/materiaEdit', materiaID]);
	}

	onDelete(materia: any): void {
		let result = confirm("Eliminar permanentemente la materia " + materia['nombre']);
  		if (result)
      	this.materiaService.delete(materia.$key);
	}

	onDetail(id: string): void {
    this.router.navigate(['/materiaDetail', id]);
  }


  onFiltroChanged(): void {
    if (this.grado)
      this.dataSource.filter = this.grado.txtNivel + " " + this.grado.grado;
    else
      this.dataSource.filter = "";
  }

}
