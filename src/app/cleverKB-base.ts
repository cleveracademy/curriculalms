import { ValidationError } from 'class-validator';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';

import { User } from 'firebase';

//Base class for every CleverKB component 
export class CleverKbBase {
	/**
	 * Method to extract the error messages from the error array of the validate method
	 */
	protected extractErrorMessages(errors: ValidationError[]): string {
    	let message = "";
    	for (let e of errors) {
    	  	for (let c in e.constraints) {
	        	message += e.constraints[c] + '\r\n';
      		}
    	}
    	return message;
	}

	/**
	* Method to validate if the user has looged in. In case there is no active user, redirect to the login page
	*/
	protected validateUser(af: AngularFireAuth, router: Router): void {
		af.authState.subscribe((user: User) => {
			if (!user)
    			router.navigate(['/login']);
  		});
	}

	protected isNotEmpty(field: string) {
		return typeof field != 'undefined' && field && field.length > 0;
	}
}