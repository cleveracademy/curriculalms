import { ElementRef } from '@angular/core';
import { MdSort } from '@angular/material';
import { BehaviorSubject, Observable } from 'rxjs';
import { DataSource } from '@angular/cdk/table';

/**
 * Class to decorate an Observable object to include filtering and sorting functionalities
 */
export class MyDataSource<T extends Object> extends DataSource<any> {
  dataHolder: DatabaseHolder<T>;
  filterChange = new BehaviorSubject('');
  get filter(): string { return this.filterChange.value; }
  set filter(filter: string) { this.filterChange.next(filter); }

  /**
   * @param data: The observable with the data that will be filtered and sorted
   * @param sort: The object with the information about the sort
   * @param filterRef: The filter element
   * @param searchFields: An array with the field names that will be used for filtering the elements inside the observable
   */
  constructor(private data: Observable<T[]>, private sort: MdSort, private filterRef: ElementRef, private searchFields: Array<string>) {
    super();

    this.dataHolder = new DatabaseHolder(data);

    Observable.fromEvent(this.filterRef.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        this.filter = this.filterRef.nativeElement.value;
      });
  }

  connect(): Observable<T[]> {
    const displayDataChanges = [
      this.dataHolder.dataChange,
      this.sort.mdSortChange,
      this.filterChange
    ];

    return Observable.merge(...displayDataChanges).map(() => {
      return this.getSortedData();
    });
  }

  disconnect() {}

  private getSortedData(): T[] {

    const data = this.dataHolder.data.slice().filter((item: T) => {
      let attributes = Object.getOwnPropertyNames(item);
      let searchStr = "";
      for (let field of this.searchFields) {
        searchStr += Reflect.get(item, field);
      }
      searchStr = searchStr.toLowerCase();
      let filters = this.filter.toLowerCase().split(' ');
      let result = true;
      for (let filter of filters) {
        result = result && (searchStr.indexOf(filter) != -1);
      }
      return  result;
    });

    if (!this.sort.active || this.sort.direction == '') { return data; }

    return data.sort((a, b) => {
      let propertyA: string = '';
      let propertyB: string = '';

      [propertyA, propertyB] = [Reflect.get(a, this.sort.active), Reflect.get(b, this.sort.active)];

      let valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      let valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this.sort.direction == 'asc' ? 1 : -1);
    });
  }
}

/**
 * Class to incorporate a subject to an observable
 */
export class DatabaseHolder<T> {
  dataChange: BehaviorSubject<T[]>;
  get data(): T[] {
    return this.dataChange.value;
  }

  constructor(dataHolder: Observable<T[]>) {
    this.dataChange = new BehaviorSubject([]);

    dataHolder.subscribe(data => {
      this.dataChange.next(data);
    });
  }
}
