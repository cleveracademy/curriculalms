import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { FirebaseListObservable } from 'angularfire2/database';
import { Router } from '@angular/router';
import { ValidationError, validate, IsNotEmpty } from 'class-validator';

import { CleverKbBase } from './../cleverKB-base';
import { NivelService } from './nivel.service';
import { FileService } from './../archivo/file.service';

@Component({
  selector: 'cleverKB-nivel',
  templateUrl: './nivel.component.html'
})

export class NivelComponent extends CleverKbBase implements OnInit {
  title = 'Niveles';
  niveles: FirebaseListObservable<any>;
  selectedNivel: any;

  constructor(private auth: AngularFireAuth, 
              private router: Router, 
              private nivelService: NivelService, 
              private fileService: FileService) {
  	super();
    super.validateUser(auth, router);
  }

  ngOnInit(): void {
    this.nivelService.getNiveles().subscribe(niveles => this.niveles = niveles);
  }

  onEdit(id: string): void {
  	this.router.navigate(['/nivelEdit', id]);
  }

  onDelete(nivel: any): void {
  	let result = confirm("Eliminar permanentemente el nivel " + nivel['nivel']);
  	if (result)
  		this.nivelService.deleteNivel(nivel.$key);
  }

  onAdd(): void {
  	this.router.navigate(['/nivelNew']);
  }

  onDetail(id: string): void {
    this.router.navigate(['/nivelDetail', id]);
  }
}