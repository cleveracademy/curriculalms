import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';

@Injectable()
export class NivelService {
	private NIVEL_TAG = "/niveles";

	constructor(private database: AngularFireDatabase) {}

	getNiveles(): FirebaseListObservable<any> {
		return this.database.list(this.NIVEL_TAG);
	}

	getNivel(id: string): FirebaseObjectObservable<any> {
		return this.database.object(this.NIVEL_TAG + '/' + id);
	}

	updateNivel(id: string, nivel: FirebaseObjectObservable<any>): void {
		this.getNivel(id).update({nivel: nivel['nivel']});
	}

	deleteNivel(id: string): void {
		this.getNivel(id).remove();
	}

	addNivel(nivel: string): void {
		this.database.list(this.NIVEL_TAG).push({nivel: nivel});
	}
}