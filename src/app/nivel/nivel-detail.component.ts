import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { AngularFireAuth } from 'angularfire2/auth';
import { FirebaseObjectObservable } from 'angularfire2/database';
import { validate, IsNotEmpty } from 'class-validator';
import { CleverKbBase } from './../cleverKB-base';
import { NivelService } from './nivel.service';

@Component ({
	selector: 'cleverKB-nivel-detail',
	templateUrl: './nivel-detail.component.html'
})

export class NivelDetailComponent extends CleverKbBase implements OnInit {
  nivel: FirebaseObjectObservable<any>;
  message: string;
  @IsNotEmpty()
  private txtNivel: string;

  constructor(private nivelService: NivelService, 
              private route: ActivatedRoute, 
              private location: Location,
              private auth: AngularFireAuth, 
              private router: Router) {
    super();
    super.validateUser(auth, router);
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.nivelService.getNivel(params['id']).subscribe(nivel => {
        this.nivel = nivel;
      });
    });
  }

  goBack(): void {
    this.location.back();
  } 
}