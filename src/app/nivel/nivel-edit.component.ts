import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { AngularFireAuth } from 'angularfire2/auth';
import { FirebaseObjectObservable } from 'angularfire2/database';
import { validate, IsNotEmpty } from 'class-validator';
import { CleverKbBase } from './../cleverKB-base';
import { NivelService } from './nivel.service';

@Component ({
	selector: 'cleverKB-nivel-edit',
	templateUrl: './nivel-edit.component.html'
})

export class NivelEditComponent extends CleverKbBase implements OnInit {
  nivel: FirebaseObjectObservable<any>;
  message: string;
  @IsNotEmpty({ message: "Nivel no puede estar vacío"})
  private txtNivel: string;

  constructor(private nivelService: NivelService, 
              private route: ActivatedRoute, 
              private location: Location,
              private auth: AngularFireAuth, 
              private router: Router) {
    super();
    super.validateUser(auth, router);
  }
  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.nivelService.getNivel(params['id']).subscribe(nivel => {
        this.nivel = nivel;
      });
    });
  }

  goBack(): void {
    this.location.back();
  }

  onUpdate(id: string): void {
    this.txtNivel = this.nivel['nivel'];
    validate(this).then(errors => {
      if (errors.length > 0) {
        this.message = super.extractErrorMessages(errors);
      }
      else {
        this.nivelService.updateNivel(id, this.nivel);
        this.router.navigate(['/niveles']);
      }
    });
  }
}