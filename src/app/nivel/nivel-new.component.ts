import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';

import { NivelService } from './nivel.service';

import { CleverKbBase } from './../cleverKB-base';
import { validate, IsNotEmpty } from 'class-validator';

@Component ({
	selector: 'cleverKB-nivel-new',
	templateUrl: './nivel-new.component.html'
})

export class NivelNewComponent extends CleverKbBase {
  @IsNotEmpty({ message: "Nivel no puede estar vacío"})
  nivel: string;
  message: string;

  constructor(private nivelService: NivelService, 
              private location: Location, 
              private router: Router, 
              private auth: AngularFireAuth) { 
    super();
    super.validateUser(auth, router);
  }

  goBack(): void {
    this.location.back();
  }

  onAdd(): void {
    validate(this).then(errors => {
      if (errors.length > 0) {
        this.message = super.extractErrorMessages(errors);
      } else {
        this.nivelService.addNivel(this.nivel);
        this.router.navigate(['/niveles']);
      }
    });
  }
}