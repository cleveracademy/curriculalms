import { Component, Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { User } from 'firebase';
import { UsuarioService } from './usuario/usuario.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

@Injectable ()
export class AppComponent {
  title = 'Administrador de la Currícula';
  username: string;

  constructor(private auth: AngularFireAuth, 
              private router: Router, 
              private usuarioService: UsuarioService) {
    this.auth.authState.subscribe((user: User) => {
      if (user) {
        usuarioService.getUserMetadata(user.uid).subscribe(usuarios => {
          this.username = usuarios[0]['nombre'];
        });
      }
      else
        this.router.navigate(['/login']);
    });
  }

  tryLogout(): void {
    this.auth.auth.signOut().then(success => {
      this.router.navigate(['/login']);
    }).catch(error => {
      this.router.navigate(['/login']);
    });
  }
}
