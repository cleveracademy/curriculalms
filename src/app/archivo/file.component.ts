import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from 'firebase';

import { FileService } from './file.service';

@Component({
	selector: 'cleverKB-file',
	templateUrl: 'file.component.html'
})

export class ArchivoComponent {
	constructor(private fileService: FileService, private auth: AngularFireAuth) {
	}

	onChange(event: EventTarget): void {
		let eventObj: MSInputMethodContext = <MSInputMethodContext> event;
        let target: HTMLInputElement = <HTMLInputElement> eventObj.target;
        let files: FileList = target.files;
        let file = files[0];
        console.log("File ", file.name, " selected");

        this.auth.authState.subscribe((user: User) => {
			let path = user.uid + "/" + file.name;
			this.fileService.uploadFile(path, file);
		});
        
	}

	onUpload(): void {
	}
}