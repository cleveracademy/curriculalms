import { Inject } from '@angular/core';
import { FirebaseApp } from 'angularfire2';
import * as firebase from 'firebase';

export class FileService {
	firebase: firebase.app.App;

	constructor(@Inject(FirebaseApp) firebase: firebase.app.App) {
		this.firebase = firebase;
	}

	uploadFile(path: string, data: any): void {
		this.firebase.storage().ref(path).put(data).then(sucess => {
			console.log(sucess.metadata.fullPath, " file successfully stored");
		});
	}
}