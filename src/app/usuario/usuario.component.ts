import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { FirebaseListObservable } from 'angularfire2/database';
import { Router } from '@angular/router';
import { ValidationError, validate, IsNotEmpty } from 'class-validator';

import { CleverKbBase } from './../cleverKB-base';
import { UsuarioService } from './usuario.service';
import { FileService } from './../archivo/file.service';

@Component({
  selector: 'cleverKB-usuario',
  templateUrl: './usuario.component.html'
})

export class UsuarioComponent extends CleverKbBase implements OnInit {
  title = 'Usuarios';
  usuarios: FirebaseListObservable<any>;
  selectedUsuario: any;

  

  constructor(private auth: AngularFireAuth, 
              private router: Router, 
              private usuarioService: UsuarioService, 
              private fileService: FileService) {
  	super();
    super.validateUser(auth, router);
  }

  ngOnInit(): void {
    this.usuarioService.getUsuarios().subscribe(usuarios => this.usuarios = usuarios);
  }

  onEdit(id: string): void {
  	this.router.navigate(['/usuarioEdit', id]);
  }

  onDelete(usuario: any): void {
  	let result = confirm("Eliminar permanentemente el usuario " + usuario.nombre + " " + usuario.apellidos);
  	if (result)
  		this.usuarioService.deleteUsuario(usuario.$key);
  }

  onAdd(): void {
  	this.router.navigate(['/usuarioNew']);
  }

  onDetail(id: string): void {
    this.router.navigate(['/usuarioDetail', id]);
  }
}