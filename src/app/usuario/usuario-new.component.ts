import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import { validate, IsNotEmpty, IsEmail } from 'class-validator';
import { CleverKbBase } from './../cleverKB-base';

import { UsuarioService } from './usuario.service';

@Component({
  selector: 'app-usuario-new',
  templateUrl: './usuario-new.component.html'
})
export class UsuarioNewComponent extends CleverKbBase implements OnInit {
  @IsNotEmpty()
	nombre: string;
  @IsNotEmpty()
  apellidos: string;
  @IsEmail()
	email: string;
  @IsNotEmpty()
	password: string;
  @IsNotEmpty()
  rol: string;
  message: string;

  //TODO: Tipos de usuario disponibles. Mover a una colección de la base de datos o a un archivo editable.
  tiposDeUsuario = [{ nombre: "Administrador de sistema", alias: "administrador"},
                    { nombre: "Administrador de contenidos", alias: "administradorContenido"},
                    { nombre: "Profesor", alias: "profesor"}];

  constructor(private location: Location, private router: Router, private usuarioService: UsuarioService) { 
    super();
  }

  ngOnInit() {
  }

  onAdd(): void {
    validate(this).then(errors => {
      if (errors.length > 0)
        this.message = super.extractErrorMessages(errors);
      else {
        this.usuarioService.addUsuario(this.nombre, this.apellidos, this.rol, this.email, this.password);
        this.router.navigate(['/']);
      }
    });
  }

  goBack(): void {
    this.location.back();
  }

}
