import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { validate, IsNotEmpty, IsEmail } from 'class-validator';
import { CleverKbBase } from './../cleverKB-base';
import { FirebaseObjectObservable } from 'angularfire2/database';
import { UsuarioService } from './usuario.service';

@Component({
  selector: 'app-usuario-edit',
  templateUrl: './usuario-edit.component.html'
})
export class UsuarioEditComponent extends CleverKbBase implements OnInit {

  usuario: FirebaseObjectObservable<any>;
  @IsNotEmpty({ message: "Nombre no puede estar vacío"})
  nombre: string;
  @IsNotEmpty({ message: "Apellidos no puede estar vacío"})
  apellidos: string;
  @IsNotEmpty({ message: "Rol no puede estar vacío"})
  rol: string;
  message: string;

  //TODO: Tipos de usuario disponibles. Mover a una colección de la base de datos o a un archivo editable.
  tiposDeUsuario = [{ nombre: "Administrador de sistema", alias: "administrador"},
                    { nombre: "Administrador de contenidos", alias: "administradorContenido"},
                    { nombre: "Profesor", alias: "profesor"}];

  constructor(private location: Location, 
              private router: Router, 
              private usuarioService: UsuarioService,
              private route: ActivatedRoute
              ) { 
    super();
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.usuarioService.getUsuario(params['id']).subscribe(usuario => {
        this.usuario = usuario;
      });
    });
  }
  
  onUpdate(id: string): void {
    this.nombre = this.usuario['nombre'];
    this.apellidos = this.usuario['apellidos'];
    this.rol = this.usuario['rol'];
    validate(this).then(errors => {
      if (errors.length > 0) {
        this.message = super.extractErrorMessages(errors);
      }
      else {
        this.usuarioService.updateUsuario(id, this.nombre, this.apellidos, this.rol);
        this.router.navigate(['/usuarios']);
      }
    });
  }

  goBack(): void {
    this.location.back();
  }

}
