import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable()
export class UsuarioService {
  private USER_TAG = '/usuarios';
  private VIGENCIA = 1;	//Años de vigencia
  private userPermissions: any;

  constructor(private auth: AngularFireAuth, 
              private database: AngularFireDatabase, 
              private http: Http) { 
    //this.http.get('src/app/bloque/user-permissions.json').subscribe(data => this.userPermissions = data.json());
  }

  /**
   * Adds a new user to the database
   */
  addUsuario(nombre: string, apellidos: string, rol: string, email: string, password: string): void {
    this.auth.auth.createUserWithEmailAndPassword(email, password).then(user => {
  		let fecha = new Date();
  		let fechaStr = fecha.getMonth()+1 + "/" + fecha.getDate() + "/" + fecha.getFullYear();
  		let vigencia = fecha.getMonth()+1 + "/" + fecha.getDate() + "/" + (fecha.getFullYear() + this.VIGENCIA);
  		this.database.list(this.USER_TAG).push({
        nombre: nombre, 
        apellidos: apellidos, 
        rol: rol, 
        fechaInicio: fechaStr, 
        vigencia: vigencia, 
        userID: user.uid});
  	});
  }

  /**
   * Obtains the data associated to a user
   */
  getUserMetadata(id: string): FirebaseListObservable<any> {
  	return this.database.list(this.USER_TAG, {query: { orderByChild: "userID", equalTo: id }});
  }

  /**
   * Obtiene la información del usuario a partir de su identificador
   */
  getUsuario(idUsuario: string): FirebaseObjectObservable<any> {
    return this.database.object(this.USER_TAG + "/" + idUsuario);
  }

  /*
   * Obtiene la lista con los usuarios disponibles en la base
   */
  getUsuarios(): FirebaseListObservable<any> {
    return this.database.list(this.USER_TAG);
  }

  updateUsuario(id: string, nombre: string, apellidos: string, rol: string): void {
    this.getUsuario(id).update({
      nombre: nombre,
      apellidos: apellidos, 
      rol: rol      
    });
  }

  deleteUsuario(idUsuario: string): void {
    this.database.object(this.USER_TAG + "/" + idUsuario).remove();
  }

  /**
   * Validates if the current user has permissions to perform the task
   * TODO: Implement this method
   */
  hasPermission(permissionName: string): boolean {
    return true;
  }
}
