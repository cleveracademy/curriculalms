import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { AngularFireAuth } from 'angularfire2/auth';
import { FirebaseObjectObservable } from 'angularfire2/database';
import { validate, IsNotEmpty } from 'class-validator';
import { CleverKbBase } from './../cleverKB-base';
import { UsuarioService } from './usuario.service';

@Component ({
	selector: 'cleverKB-usuario-detail',
	templateUrl: './usuario-detail.component.html'
})

export class UsuarioDetailComponent extends CleverKbBase implements OnInit {
  usuario: FirebaseObjectObservable<any>;
  message: string;
  @IsNotEmpty()
  private txtNivel: string;

  constructor(private usuarioService: UsuarioService, 
              private route: ActivatedRoute, 
              private location: Location,
              private auth: AngularFireAuth, 
              private router: Router) {
    super();
    super.validateUser(auth, router);
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.usuarioService.getUsuario(params['id']).subscribe(usuario => {
        this.usuario = usuario;
      });
    });
  }

  goBack(): void {
    this.location.back();
  } 
}