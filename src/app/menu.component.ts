import { Component, Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';   //Angular modules
import { Router } from '@angular/router';
import { User } from 'firebase';

@Component({
  selector: 'cleverKB-menu',
  templateUrl: 'menu.component.html'
})

export class MenuComponent {

  constructor(private auth: AngularFireAuth, 
              private router: Router) {
    this.auth.authState.subscribe((user: User) => {
  		/*
      if (user) {
  			document.getElementById('menuPrincipal').removeAttribute('hidden');
        //document.getElementById('menuGrupos').removeAttribute('hidden');
        document.getElementById('menuBloques').removeAttribute('hidden');
        document.getElementById('menuNiveles').removeAttribute('hidden');
        document.getElementById('menuGrados').removeAttribute('hidden');
        document.getElementById('menuMaterias').removeAttribute('hidden');
        //document.getElementById('menuIntereses').removeAttribute('hidden');
        document.getElementById('menuUsuarioNew').removeAttribute('hidden');
        //document.getElementById('menuArchivos').removeAttribute('hidden');
      }
      else {
        //console.log("Invalid user... going to login page");
        document.getElementById('menuPrincipal').setAttribute('hidden', 'true');
        //document.getElementById('menuGrupos').setAttribute('hidden', 'true');
        document.getElementById('menuBloques').setAttribute('hidden', 'true');
        document.getElementById('menuNiveles').setAttribute('hidden', 'true');
        document.getElementById('menuGrados').setAttribute('hidden', 'true');
        document.getElementById('menuMaterias').setAttribute('hidden', 'true');
        document.getElementById('menuIntereses').setAttribute('hidden', 'true');
        document.getElementById('menuUsuarioNew').setAttribute('hidden', 'true');
        document.getElementById('menuArchivos').setAttribute('hidden', 'true');
        this.router.navigate(['/login']);
      }
      */
  	});
  }

  tryLogout(): void {
    this.auth.auth.signOut().then(success => {
      //console.log("Successfull logout");
      this.router.navigate(['/login']);
    }).catch(error => {
      //console.log("No user detected");
      this.router.navigate(['/login']);
    });
  }
}