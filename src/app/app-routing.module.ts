import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//App components
import { LoginComponent } from './login.component';
import { MainComponent } from './main.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { UsuarioNewComponent } from './usuario/usuario-new.component';
import { UsuarioEditComponent } from './usuario/usuario-edit.component';

import { NivelComponent } from './nivel/nivel.component';
import { NivelDetailComponent } from './nivel/nivel-detail.component';
import { NivelEditComponent } from './nivel/nivel-edit.component';
import { NivelNewComponent } from './nivel/nivel-new.component';

import { GradoComponent } from './grado/grado.component';
import { GradoDetailComponent } from './grado/grado-detail.component';
import { GradoEditComponent } from './grado/grado-edit.component';
import { GradoNewComponent } from './grado/grado-new.component';

import { BloqueComponent } from './bloque/bloque.component';
import { BloqueEditComponent } from './bloque/bloque-edit.component';
import { BloqueNewComponent } from './bloque/bloque-new.component';
import { BloqueDetailComponent } from './bloque/bloque-detail.component';
import { BloqueResolve } from './bloque/bloque.resolve';

import { GrupoComponent } from './grupo/grupo.component';
import { GrupoDetailComponent } from './grupo/grupo-detail.component';
import { GrupoEditComponent } from './grupo/grupo-edit.component';
import { GrupoNewComponent } from './grupo/grupo-new.component';

import { MateriaComponent } from './materia/materia.component';
import { MateriaDetailComponent } from './materia/materia-detail.component';
import { MateriaEditComponent } from './materia/materia-edit.component';
import { MateriaNewComponent } from './materia/materia-new.component';

import { InteresComponent } from './interes/interes.component';
import { InteresNewComponent } from './interes/interes-new.component';
import { InteresDetailComponent } from './interes/interes-detail.component';

import { ArchivoComponent } from './archivo/file.component';

//Routes
const routes: Routes = [
  { path: '', redirectTo: '/main', pathMatch: 'full'},
  { path: 'login', component: LoginComponent },
  { path: 'main', component: MainComponent },
  
  { path: 'usuarios', component: UsuarioComponent },
  { path: 'usuarioNew', component: UsuarioNewComponent },
  { path: 'usuarioEdit/:id', component: UsuarioEditComponent },

  { path: 'niveles', component: NivelComponent },
  { path: 'nivelDetail/:id', component: NivelDetailComponent },
  { path: 'nivelEdit/:id', component: NivelEditComponent },
  { path: 'nivelNew', component: NivelNewComponent },
  
  { path: 'grados', component: GradoComponent },
  { path: 'gradoNew', component: GradoNewComponent },
  { path: 'gradoDetail/:id', component: GradoDetailComponent },
  { path: 'gradoEdit/:id', component: GradoEditComponent },

  { path: 'bloques', component: BloqueComponent },
  { path: 'bloqueNew', component: BloqueNewComponent, resolve: { messages: BloqueResolve } },
  { path: 'bloqueDetail/:id', component: BloqueDetailComponent },
  { path: 'bloqueEdit/:id', component: BloqueEditComponent, resolve: { messages: BloqueResolve } },

  { path: 'grupos', component: GrupoComponent },
  { path: 'grupoNew', component: GrupoNewComponent },
  { path: 'grupoDetail/:id', component: GrupoDetailComponent },
  { path: 'grupoEdit/:id', component: GrupoEditComponent },

  { path: 'materias', component: MateriaComponent },
  { path: 'materiaNew', component: MateriaNewComponent },
  { path: 'materiaDetail/:id', component: MateriaDetailComponent },
  { path: 'materiaEdit/:id', component: MateriaEditComponent },

  { path: 'intereses', component: InteresComponent },
  { path: 'interesNew', component: InteresNewComponent },
  { path: 'interesDetail/:id', component: InteresDetailComponent },
  { path: 'archivo', component: ArchivoComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
