import { Component, Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'cleverKB-main',
  templateUrl: 'main.component.html'
})

export class MainComponent {
  title = 'Bienvenido al SACC -  Sistema para la Administración de la Currícula Clever';
}