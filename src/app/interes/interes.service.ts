import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';

import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { MateriaService } from './../materia/materia.service';

@Injectable()
export class InteresService {
	private INTERES_TAG = "/intereses";
	private MATERIAS_INTERES_TAG = "/materiasInteres";
	private RECURSOS_INTERES_TAG = "/recursosInteres";
	private TERMINOS_INTERES_TAG = "/terminosInteres";

	constructor(private database: AngularFireDatabase, private materiaService: MateriaService) {}

	getIntereses(): Observable<any> {
		return this.database.list(this.INTERES_TAG).map(intereses => {
			return intereses.map(interes => {
				this.materiaService.getMateria(interes.materia).subscribe(materia => {
					interes.txtMaterias = materia.nombre;
				});
				return interes;
			});
			
		});
	}

	getInteres(id: string): FirebaseObjectObservable<any> {
		return this.database.object(this.INTERES_TAG + '/' + id);
	}

	update(id: string, interes: FirebaseObjectObservable<any>): void {
		this.getInteres(id).update({clave: interes['clave'], 
									descripcion: interes['descripcion']});
	}

	delete(id: string): void {
		this.getInteres(id).remove();
	}

	create(clave: string, descripcion: string, materias: string[], recursos: string[], terminos: string[]): void {
		let materiasInteres: FirebaseListObservable<any>;
		let recursosInteres: FirebaseListObservable<any>;
		let terminosInteres: FirebaseListObservable<any>;

		let interesRef = this.database.list(this.INTERES_TAG).push({clave: clave, descripcion: descripcion});
		
		materiasInteres = this.database.list(this.MATERIAS_INTERES_TAG);
		for (let materia of materias) {
			materiasInteres.push({interes: interesRef.key, materia: materia});
		}

		recursosInteres = this.database.list(this.RECURSOS_INTERES_TAG);
		for (let recurso of recursos) {
			recursosInteres.push({interes: interesRef.key, recurso: recurso});
		}
	}

}
