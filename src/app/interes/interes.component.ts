import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { ValidationError, validate, IsNotEmpty } from 'class-validator';

import { CleverKbBase } from './../cleverKB-base';
import { InteresService } from './interes.service';

import { Observable } from 'rxjs';

@Component({
  selector: 'app-interes',
  templateUrl: './interes.component.html'
})
export class InteresComponent extends CleverKbBase implements OnInit {
	title = "Intereses";
	intereses: Observable<any>;

	constructor(private auth: AngularFireAuth, private router: Router, private interesService: InteresService) {
  		super();
    	super.validateUser(auth, router);
  	}

	ngOnInit() {
		this.interesService.getIntereses().subscribe(intereses => this.intereses = intereses);
	}

	onEdit(id: string): void {
		this.router.navigate(['/interesDetail', id]);
	}

	onDelete(interes: any): void {
		let result = confirm("Eliminar permanentemente el interes " + interes['clave']);
  		if (result)
      		this.interesService.delete(interes.$key);
	}

	onAdd(): void {
		this.router.navigate(['/interesNew']);
	}
}
