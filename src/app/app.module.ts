import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';   //Angular modules
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { AppRoutingModule } from './app-routing.module';                        //For routing
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule, MdTableModule, MdPaginatorModule, MdSortModule, MdInputModule } from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';

//App components
import { AppComponent } from './app.component';
import { LoginComponent } from './login.component';
import { MainComponent } from './main.component';
import { MenuComponent } from './menu.component';
//App services
import { NivelService } from './nivel/nivel.service';
import { GradoService } from './grado/grado.service';
import { FileService } from './archivo/file.service';
import { UsuarioService } from './usuario/usuario.service';
import { MateriaService } from './materia/materia.service';
import { InteresService } from './interes/interes.service';
import { GrupoService   } from './grupo/grupo.service';
import { BloqueService  } from './bloque/bloque.service';
import { TemaService } from './bloque/tema.service';
//Niveles
import { NivelComponent } from './nivel/nivel.component';
import { NivelDetailComponent } from './nivel/nivel-detail.component';
import { NivelEditComponent } from './nivel/nivel-edit.component';
import { NivelNewComponent } from './nivel/nivel-new.component';
//Grados
import { GradoComponent } from './grado/grado.component';
import { GradoDetailComponent } from './grado/grado-detail.component';
import { GradoEditComponent } from './grado/grado-edit.component';
import { GradoNewComponent } from './grado/grado-new.component';

import { ArchivoComponent } from './archivo/file.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { UsuarioDetailComponent } from './usuario/usuario-detail.component';
import { UsuarioEditComponent } from './usuario/usuario-edit.component';
import { UsuarioNewComponent } from './usuario/usuario-new.component';

//Bloques
import { BloqueComponent } from './bloque/bloque.component';
import { BloqueEditComponent } from './bloque/bloque-edit.component';
import { BloqueDetailComponent } from './bloque/bloque-detail.component';
import { BloqueNewComponent } from './bloque/bloque-new.component';
import { BloqueResolve } from './bloque/bloque.resolve';

//Temas
import { EditorElementosTema } from './bloque/editorElementosTema.component';
//Grupos
import { GrupoComponent } from './grupo/grupo.component';
import { GrupoDetailComponent } from './grupo/grupo-detail.component';
import { GrupoEditComponent } from './grupo/grupo-edit.component';
import { GrupoNewComponent } from './grupo/grupo-new.component';

//Materias
import { MateriaComponent } from './materia/materia.component';
import { MateriaDetailComponent } from './materia/materia-detail.component';
import { MateriaEditComponent } from './materia/materia-edit.component';
import { MateriaNewComponent } from './materia/materia-new.component';

//Intereses
import { InteresComponent } from './interes/interes.component';
import { InteresNewComponent } from './interes/interes-new.component';
import { InteresDetailComponent } from './interes/interes-detail.component';

// Firebase configuracion
export const firebaseConfig = {
  apiKey: "",
  authDomain: "",
  databaseURL: "",
  projectId: "",
  storageBucket: "",
  messagingSenderId: ""
};

@NgModule({
  declarations: [ AppComponent, LoginComponent, MainComponent, MenuComponent,
                  NivelComponent, NivelEditComponent, NivelDetailComponent, NivelNewComponent,
                  GradoComponent, GradoEditComponent, GradoDetailComponent, GradoNewComponent,
                  ArchivoComponent, UsuarioComponent, UsuarioNewComponent, UsuarioDetailComponent, UsuarioEditComponent,
                  MateriaComponent, MateriaEditComponent, MateriaDetailComponent, MateriaNewComponent,
                  InteresComponent, InteresNewComponent, InteresDetailComponent,
                  GrupoComponent, GrupoEditComponent, GrupoNewComponent, GrupoDetailComponent,
                  BloqueComponent, BloqueNewComponent, BloqueEditComponent, BloqueDetailComponent,
                  EditorElementosTema ],
  entryComponents: [ EditorElementosTema ],
  imports:      [ BrowserModule, FormsModule, HttpModule, AppRoutingModule, MaterialModule, NoopAnimationsModule,
                  AngularFireModule.initializeApp(firebaseConfig), AngularFireDatabaseModule, AngularFireAuthModule,
                  MdTableModule, CdkTableModule, MdPaginatorModule, MdSortModule, MdInputModule
                ],
  providers:    [ NivelService, GradoService, FileService, UsuarioService, MateriaService, InteresService,
                  BloqueService, GrupoService, TemaService,
                  BloqueResolve ],
  bootstrap:   [ AppComponent ]
})
export class AppModule { }
