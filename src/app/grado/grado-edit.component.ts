import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { AngularFireAuth } from 'angularfire2/auth';
import { FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { validate, IsNotEmpty } from 'class-validator';
import 'rxjs/add/operator/switchMap';
import { CleverKbBase } from './../cleverKB-base';
import { NivelService } from './../nivel/nivel.service';
import { GradoService } from './grado.service';

@Component ({
	selector: 'cleverKB-grado-edit',
	templateUrl: './grado-edit.component.html'
})

export class GradoEditComponent extends CleverKbBase implements OnInit {
  grado: FirebaseObjectObservable<any>;
  niveles: FirebaseListObservable<any>;
  message: string;
  @IsNotEmpty({ message: "Grado no puede estar vacío"})
  private txtGrado: string;
  @IsNotEmpty({ message: "Selecciona un nivel"})
  private txtNivel: string;
  constructor(private nivelService: NivelService, 
              private gradoService: GradoService, 
              private route: ActivatedRoute, 
              private location: Location,
              private auth: AngularFireAuth,  
              private router: Router) {
    super();
    super.validateUser(auth, router);
  }

  ngOnInit(): void {
    this.nivelService.getNiveles().subscribe(niveles => this.niveles = niveles);
    this.route.params.subscribe((params: Params) => {
      this.gradoService.getGrado(params['id']).subscribe(grado => {
        this.grado = grado;
      });
    });
  }

  goBack(): void {
    this.location.back();
  }

  onUpdate(id: string): void {
    this.txtGrado = this.grado['grado'];
    this.txtNivel = this.grado['nivel'];
    validate(this).then(errors => {
      if (errors.length > 0) {
        this.message = super.extractErrorMessages(errors);
      }
      else {
        this.gradoService.updateGrado(id, this.grado);
        this.router.navigate(['/grados']);


        
      }
    });
  }




}