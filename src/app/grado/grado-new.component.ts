import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { FirebaseListObservable } from 'angularfire2/database';
import { NivelService } from './../nivel/nivel.service';
import { GradoService } from './grado.service';

import { CleverKbBase } from './../cleverKB-base';
import { validate, IsNotEmpty } from 'class-validator';


@Component ({
	selector: 'cleverKB-grado-new',
	templateUrl: './grado-new.component.html'
})

export class GradoNewComponent extends CleverKbBase {
  @IsNotEmpty({ message: "Grado no puede estar vacío"})
  grado: string;
  @IsNotEmpty({ message: "Nivel no puede estar vacío"})
  nivel: string;
  message: string;
  
  niveles: FirebaseListObservable<any>;

  constructor(private gradoService: GradoService, 
              private nivelService: NivelService, 
              private location: Location, 
              private router: Router, 
              private auth: AngularFireAuth) {
    super();
    super.validateUser(auth, router);
    nivelService.getNiveles().subscribe(niveles => this.niveles = niveles);
  }

  goBack(): void {
    this.location.back();
  }

  onAdd(): void {
    validate(this).then(errors => {
      if (errors.length > 0) {
        this.message = super.extractErrorMessages(errors);
      } else {
        this.gradoService.addGrado(this.grado, this.nivel);
        this.router.navigate(['/grados']);
      }
    });
  }



}