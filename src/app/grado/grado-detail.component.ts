import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2/database';
import 'rxjs/add/operator/switchMap';
import { NivelService } from './../nivel/nivel.service';
import { GradoService } from './grado.service';

@Component ({
	selector: 'cleverKB-grado-detail',
	templateUrl: './grado-detail.component.html'
})

export class GradoDetailComponent implements OnInit {
  grado: FirebaseObjectObservable<any>;
  niveles: FirebaseListObservable<any>;

  constructor(private nivelService: NivelService, 
              private gradoService: GradoService, 
              private route: ActivatedRoute, 
              private location: Location, 
              private router: Router) {}

  ngOnInit(): void {
    this.nivelService.getNiveles().subscribe(niveles => this.niveles = niveles);
    this.route.params.subscribe((params: Params) => {
      this.gradoService.getGrado(params['id']).subscribe(grado => {
        this.grado = grado;
      });
    });
  }

  goBack(): void {
    this.location.back();
  }


}