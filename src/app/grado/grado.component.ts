import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { ValidationError, validate, IsNotEmpty } from 'class-validator';

import { CleverKbBase } from './../cleverKB-base';
import { GradoService } from './grado.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'cleverKB-grado',
  templateUrl: './grado.component.html'
})

export class GradoComponent extends CleverKbBase implements OnInit {
  title = 'Grados';
  grados: Observable<any>;

  constructor(private auth: AngularFireAuth, 
              private router: Router, 
              private gradoService: GradoService) {
  	super();
    super.validateUser(auth, router);
  }

  ngOnInit(): void {
    this.gradoService.getGrados().subscribe(grados => {
      this.grados = grados;
    });
  }

  onEdit(id: string): void {
  	this.router.navigate(['/gradoEdit', id]);
  }

  onDelete(grado: any): void {
  	let result = confirm("Eliminar permanentemente el grado " + grado['grado']);
  	if (result)
      this.gradoService.deleteGrado(grado.$key);
  }

  onAdd(): void {
  	this.router.navigate(['/gradoNew']);
  }
  onDetail(id: string): void {
    this.router.navigate(['/gradoDetail', id]);
  }
}