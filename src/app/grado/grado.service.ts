import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';

import { NivelService } from './../nivel/nivel.service';

import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class GradoService {
	private GRADOS_TAG = "/grados";

	constructor(private database: AngularFireDatabase, private nivelService: NivelService) {}

	getGrados(): Observable<any> {
		return this.database.list(this.GRADOS_TAG).map(grados => {
			return grados.map(grado => {
				this.nivelService.getNivel(grado.nivel).subscribe(nivel => {
					grado.txtNivel = nivel.nivel;
				});
				return grado;
			});
			
		});
	}

	getGrado(id: string): Observable<any> {
		return this.database.object(this.GRADOS_TAG + '/' + id).map(grado => {
			this.nivelService.getNivel(grado.nivel).subscribe(nivel => {
				grado.txtNivel = nivel.nivel;
			});
			
			return grado;
		});
	}

	updateGrado(id: string, grado: FirebaseObjectObservable<any>): void {
		this.database.object(this.GRADOS_TAG + '/' + id).update({grado: grado['grado'], nivel: grado['nivel']});
	}

	deleteGrado(id: string): void {
		this.database.object(this.GRADOS_TAG + '/' + id).remove();
	}

	addGrado(grado: string, nivel: string): void {
		this.database.list(this.GRADOS_TAG).push({grado: grado, nivel: nivel});
	}
}