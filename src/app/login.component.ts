import { Component, Injectable } from '@angular/core';
import { CleverKbBase } from './cleverKB-base';
import { AngularFireAuth } from 'angularfire2/auth';   //Angular modules
import { Router } from '@angular/router';

import { validate, IsEmail, IsNotEmpty } from 'class-validator';

@Component({
  selector: 'cleverKB-login',
  templateUrl: './login.component.html'
})

@Injectable ()
export class LoginComponent extends CleverKbBase {
  title = 'Login';
  
  @IsEmail()
  email: string;
  @IsNotEmpty({message: 'Contraseña inválida'})
  password: string;

  message: string;

  constructor(private afAuth: AngularFireAuth, private router: Router) {
    super();
  }
  
  tryLogin(): void {
    validate(this).then(errors => {
      if (errors.length > 0) {
        this.message = super.extractErrorMessages(errors);
      }
      else {
    	  this.afAuth.auth.signInWithEmailAndPassword(this.email, this.password)
          .then(sucess => {
    		    this.router.navigate(['/main']);
  	      }).catch(error => {
  		      this.message = error.message;
  	      });
      }
    });
  }
}
