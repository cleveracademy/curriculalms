import { BehaviorSubject, Observable } from 'rxjs';

/**
 * Class to incorporate pagination to an observable.
 * Usage:
 * 	let paged:PagedObservable = new PagedObservable();
 * 	let list:Observable = db.list(MY_LIST_TAG, {query: paged.query});
 * 	paged.setObservable(list);
 *
 * nextPage(): void {
 *	this.paged.nextPage();
 * }
 *
 * previousPage(): void {
 *	this.paged.previousPage();
 * }
 */
export class PagedObservable {
	/** Number of elements of the pages */
	PAGE_SIZE = 10;
	/** Key of the first element in the page */
	firstKey: BehaviorSubject<any>;
	private nextKey: BehaviorSubject<any>;
	private previousKey: BehaviorSubject<any>;
	private keyStack: Map<number, string>;
	private currentPage: number;
	/** True when the first page is reached */
	private disableFirstPage = true;
	/** True when the last page is reached */
	private disableLastPage = false;
	query: any;

	constructor(pageSize?: number) {
		if (pageSize)
			this.PAGE_SIZE = pageSize;

		this.firstKey = new BehaviorSubject('');
	  	this.nextKey = new BehaviorSubject('');
	  	this.previousKey = new BehaviorSubject('');
	  	this.keyStack = new Map();
	  	this.currentPage = 0;

	  	this.query = {orderByKey: true, startAt: this.firstKey, limitToFirst: this.PAGE_SIZE};
	}

	/**
	 * Determines the observable that will be used for pagination purposes
	 */
	setObservable(observable: Observable<any>): void {
		observable.subscribe(data => {
			//Store the first element of the page
			if (!this.keyStack[this.currentPage])
				this.keyStack[this.currentPage] = data[0].$key;

			//Check first and last pages
			this.disableFirstPage = this.currentPage == 0;
			this.disableLastPage = data.length !== this.PAGE_SIZE;

			//Obtain the next key
			this.previousKey.next((this.currentPage > 0) ? this.keyStack[this.currentPage-1] : '');
			this.nextKey.next((data.length === this.PAGE_SIZE) ? data[data.length-1].$key : '');
		});
	}

	/**
	 * Moves to the next page of the observable
	 */
	nextPage(): void {
		this.currentPage++;
  		this.firstKey.next(this.nextKey.getValue());
	}

	/**
	 * Moves to the previous page of the observable 
	 */
	previousPage(): void {
		this.currentPage--;
  		this.firstKey.next(this.previousKey.getValue());		
	}

	/**
	 * Obtains the page number of the current page
	 */
	getCurrentPage(): number {
		return this.currentPage + 1;
	}

	isDisabledFirstPage(): boolean {
		return this.disableFirstPage;
	}

	isDisabledLastPage(): boolean {
		return this.disableLastPage;
	}
}

/**
 * Interface to be implemented by any service that wants paging functionality
 */
export interface IPageable {
	nextPage(): void;
	previousPage(): void;
	isDisabledFirstPage(): boolean;
	isDisabledLastPage(): boolean;
}

/**
 * Class to implement paging functionality to a service
 */
export class Pageable implements IPageable {
	protected pager: PagedObservable;

	/**
	 * @param pageSize: Establishes the number of elements on every page
	 */
	constructor(pageSize?: number) {
		this.pager = new PagedObservable(pageSize);
	}

	nextPage(): void {
  		this.pager.nextPage();
  	}

  	previousPage(): void {
  		this.pager.previousPage();
  	}

  	isDisabledFirstPage(): boolean {
  		return this.pager.isDisabledFirstPage();
  	}

  	isDisabledLastPage(): boolean {
  		return this.pager.isDisabledLastPage();
  	}
}